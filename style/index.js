import React from 'react';
import styled from 'styled-components';
import {
  Button,
  Text,
  Input,
  Radio,
  Autocomplete,
  TabBar,
  Icon,
  Tab,
} from '@ui-kitten/components';
import {
  KeyboardAvoidingView,
  View,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export const WideButton = styled(Button)`
  font-family: 'Nunito-Regular';
  background-color: ${({theme: {colors}}) => colors[6]};
  border-color: ${({theme: {colors}}) => colors[6]};
  padding: 20px;
  width: 90%;
  margin: 5px;
  justify-content: center;
`;

export const CenterView = styled(View)`
  align-items: center;
  justify-content: center;
  height: 100%;
  margin-top: 80px;
`;

export const StyledGradient = styled(LinearGradient).attrs(
  ({theme: {colors}, colorList}) => ({
    colors: [colors[colorList[0]], colors[colorList[1]]],
  }),
)`
  flex: 1;
  justify-content: space-around;
  align-items: center;
`;

export const ColorText = styled(Text)`
  font-family: 'Nunito-Bold';
  color: ${({theme: {colors}, color}) => colors[color]};
`;

export const RenderView = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: ${({theme: {colors}}) => colors[2]};
`;

export const RowView = styled(View)`
  flex-direction: row;
`;

export const StyledGradientList = styled(LinearGradient).attrs(
  ({theme: {colors}, colorList}) => ({
    colors: [colors[colorList[0]], colors[colorList[1]]],
  }),
)`
  flex: 1;
  align-items: center;
  padding: 5px;
`;

export const SwipeListItem = styled(View)`
  align-items: center;
  background-color: ${({theme: {colors}}) => colors[1]};
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
`;

export const MarginedInput = styled(Input)`
  font-family: 'Nunito-Bold';
  margin: 15px;
`;

export const MarginedRadio = styled(Radio)`
  margin: 15px;
`;

export const MarginedAutocomplete = styled(Autocomplete)`
  margin: 15px;
`;

export const MarginTopView = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 30px;
`;

export const WidthButton = styled(Button)`
  font-family: 'Nunito-Regular';
  width: ${({width}) => `${width}px`};
`;

export const StyledTabBar = styled(TabBar)`
  width: 100%;
  margin-top: 20px;
  background-color: ${({theme: {colors}}) => colors[2]};
`;

export const LearningLayout = styled(SafeAreaView)`
  flex: 1;
  align-items: center;
  margin: 0px;
  padding: 30px;
  background-color: ${({theme: {colors}}) => colors[2]};
`;

export const ListItemCheckbox = styled(View)`
  width: 300px;
  flex-direction: row;
  margin: 10px;
  padding: 10px;
  justify-content: space-around;
  border-radius: 10px;
  background-color: ${({theme: {colors}, isSelected}) => colors[isSelected ? 3 : 1]};
`;

export const WordItem = styled(Text)`
  color: ${({theme: {colors}}) => colors[1]};
  font-family: 'Nunito-Bold';
  border-width: 2px;
  border-color: ${({theme: {colors}}) => colors[1]};
  border-radius: 5px;
  padding: 30px;
  margin: 10px;
`;

export const TopLeftCircle = styled(View)`
  background-color: ${({theme: {colors}}) => colors[2]};
  position: absolute;
  top: 0;
  left: 0;
  border-bottom-right-radius: 500px;
`;
export const TopRightCircle = styled(View)`
  background-color: ${({theme: {colors}}) => colors[3]};
  position: absolute;
  top: 0;
  right: 0;
  border-bottom-left-radius: 500px;
`;

export const MainHeader = styled(Text)`
  font-family: 'Nunito-Bold';
  color: ${({theme: {colors}, color}) => colors[color]};
  position: absolute;
  top: 40px;
  left: 40px;
`;

export const SecondHeader = styled(Text)`
  font-family: 'Nunito-Bold';
  color: ${({theme: {colors}, color}) => colors[color]};
  position: absolute;
  top: 40px;
  right: ${({right}) => `${right}px`};
  width: 50%;
`;

export const StyledBackgroundImage = styled(ImageBackground)`
  width: 100%;
  height: 100%;
  flex: 1;
`;

export const DarkBackground = styled(KeyboardAvoidingView)`
  background-color: ${({theme: {colors}}) => colors[2]};
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 20px;
`;

export const ArrowBackLeft = styled(Button).attrs(() => ({
  accessoryLeft: Arrow,
}))`
  position: absolute;
  left: 20px;
  top: 30px;
  background-color: ${({theme: {colors}}) => colors[2]};
  border-color: ${({theme: {colors}}) => colors[2]};
`;

export const GreenButton = styled(Button)`
  font-family: 'Nunito-Regular';
  background-color: ${({theme: {colors}}) => colors[3]};
  border-color: ${({theme: {colors}}) => colors[3]};
  padding: 20px;
  width: 90%;
  margin: 5px;
  justify-content: center;
  position: absolute;
  bottom: 10px;
`;

export const StyledText = styled(Text)`
  font-family: 'Nunito-Bold';
`;

export const ColoredButton = styled(Button)`
  background-color: ${({theme: {colors}, color}) => colors[color]};
  border-color: ${({theme: {colors}, color}) => colors[color]};
`;

export const RowForButtons = styled(View)`
  justify-content: space-between;
  flex-direction: row;
  width: 100%;
  position: absolute;
  bottom: ${({marginBottom}) => `${marginBottom}px`};
  align-items: center;
`;

const Arrow = (props) => (
  <Icon
    {...props}
    name="arrow-back-outline"
    fill="#8F9BB3"
    style={{width: 30, height: 30}}
  />
);

export const StyledTab = styled(Tab)`
  background-color: ${({theme: {colors}, color}) => colors[color]};
  padding: 10px;
  border-radius: 5px;
`;
