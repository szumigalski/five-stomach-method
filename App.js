import React, {useState, useEffect} from 'react';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {
  MainHeader,
  TopLeftCircle,
  WideButton,
  CenterView,
  ColorText,
  StyledBackgroundImage,
} from './style';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import NewWordsComponent from './components/NewWordsComponent/NewWordsComponent';
import LearningComponent from './components/LearningComponent/LearningComponent';
import StomachComponent from './components/StomachComponent/StomachComponent';
import EditComponent from './components/EditComponent/EditComponent';
import InstructionComponent from './components/InstructionComponent/InstructionComponent';
import {CategorySchema, WordSchema} from './components/schemas';
import Toast from 'react-native-simple-toast';
import AnimatedSplash from 'react-native-animated-splash-screen';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {ThemeProvider} from 'styled-components';
import {theme} from './style/theme';
import GlobalFont from 'react-native-global-font';
import {Image, View, Dimensions} from 'react-native';
import background from './assets/background.png';
import logo from './assets/logo.png';
import { words, categories} from './words'
import Flag from 'react-native-flags';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import langEng from './components/languages/eng.json'
import langPl from './components/languages/pl.json'

const Realm = require('realm');

const HomeScreen = ({navigation}) => {
  const [isLoading, setLoading] = useState(false);
  const [language, setLanguage] = useState('en')

  useEffect(() => {
    setTimeout(() => {
      setLoading(true);
    }, 300);
    let fontName = 'Nunito-Bold';
    GlobalFont.applyGlobal(fontName);
  }, []);

  useEffect(() => {
    const getData = async () => {
      try {
        const value = await AsyncStorage.getItem('lang')
        if(value !== null) {
          setLanguage(value)
        }
      } catch(e) {
        // error reading value
      }
    }
    getData()
  }, [])

  const translate = (value) => {
    if(language === 'pl') {
      return langPl[value]
    }
    return langEng[value]
  }

  const storeLang = async (value) => {
    setLanguage(value)
    try {
      await AsyncStorage.setItem('lang', value)
    } catch (e) {
      // saving error
    }
  }

  const addMockItems = () => {
    Realm.open({schema: [WordSchema, CategorySchema]}).then((realm) => {
      realm.write(() => {
        categories.map((category, i) => {
          realm.create('Category', {
            id: category,
            name: category,
            isSelected: false,
            1: words[i].length,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0
          });
          words[i].map((word) => {
            realm.create('Word', {
              id: Math.floor(Math.random() * 5000000),
              word: word.word,
              translate: word.translate,
              category: category,
              stomachNumber: 1,
            });
          });
        });
      });
    });
  };

  return (
    <AnimatedSplash
      translucent={true}
      isLoaded={isLoading}
      logoImage={logo}
      backgroundColor={'#656666'}
      logoHeight={150}
      logoWidth={150}>
      <StyledBackgroundImage source={background}>
        <View style={{padding: 30}}>
          <TopLeftCircle
            width={(Dimensions.get('window').width / 3) * 2}
            height={(Dimensions.get('window').width / 3) * 2}
          />
          <Image
            source={logo}
            style={{
              width: 100,
              height: 100,
              position: 'absolute',
              top: 150,
              right: 30,
            }}
          />
          <MainHeader category="h4" color={1} style={{width: '50%'}}>
           {translate('five_stomach_method')}
          </MainHeader>
          <View style={{ position: 'absolute', top: 25, right: 10 }}>
            <TouchableOpacity onPress={() => storeLang('en')}>
              <Flag
                code="GB"
                type='flat'
                size={32}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => storeLang('pl')}>
            <Flag
              code="PL"
              type='flat'
              size={32}
            />
            </TouchableOpacity>
          </View>
          <CenterView>
            <WideButton
              size="giant"
              onPress={() => navigation.navigate('Nowe słówko', { translate: (word) => translate(word) })}>
              <ColorText color={1}>{translate('new_words')}</ColorText>
            </WideButton>
            <WideButton
              size="giant"
              onPress={() => navigation.navigate('Wybierz kategorie', { translate: (word) => translate(word) })}>
              <ColorText color={1}>{translate('learning')}</ColorText>
            </WideButton>
            <WideButton
              size="giant"
              onPress={() => navigation.navigate('Edycja', { translate: (word) => translate(word) })}>
              <ColorText color={1}>{translate('edition')}</ColorText>
            </WideButton>
            <WideButton
              onPress={() => {
                Toast.showWithGravity(
                  translate('add_custom_words_and_categories'),
                  Toast.LONG,
                  Toast.BOTTOM,
                );
                addMockItems();
              }}>
              <ColorText color={1}>{translate('test_words_and_categories')}</ColorText>
            </WideButton>
            <WideButton
              size="giant"
              onPress={() => navigation.navigate('Instrukcja', { translate: (word) => translate(word) })}>
              <ColorText color={1}>{translate('instruction')}</ColorText>
            </WideButton>
          </CenterView>
        </View>
      </StyledBackgroundImage>
    </AnimatedSplash>
  );
};

const Stack = createStackNavigator();

export default () => (
  <>
    <IconRegistry icons={EvaIconsPack} />
    <ApplicationProvider {...eva} theme={eva.light}>
      <ThemeProvider theme={theme}>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName="Metoda pięciu żołądków"
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen
              name="Metoda pięciu żołądków"
              component={HomeScreen}
            />
            <Stack.Screen name="Nowe słówko" component={NewWordsComponent} />
            <Stack.Screen
              name="Wybierz kategorie"
              component={LearningComponent}
            />
            <Stack.Screen name="Nauka" component={StomachComponent} />
            <Stack.Screen name="Edycja" component={EditComponent} />
            <Stack.Screen name="Instrukcja" component={InstructionComponent} />
          </Stack.Navigator>
        </NavigationContainer>
      </ThemeProvider>
    </ApplicationProvider>
  </>
);
