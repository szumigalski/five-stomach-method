export const CategorySchema = {
  name: 'Category',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    isSelected: 'bool',
    1: 'int',
    2: 'int',
    3: 'int',
    4: 'int',
    5: 'int',
    6: 'int'
  },
};
export const WordSchema = {
  name: 'Word',
  primaryKey: 'id',
  properties: {
    id: 'int',
    word: 'string',
    translate: 'string',
    category: 'string',
    stomachNumber: 'int',
  },
};
