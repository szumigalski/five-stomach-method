import React, {useEffect, useState} from 'react';
import {Text, Button, Spinner, AutocompleteItem} from '@ui-kitten/components';
import {CategorySchema, WordSchema} from '../schemas';
import {View} from 'react-native'
import _ from 'lodash';
import {
  ColorText,
  MarginedAutocomplete,
  StyledTabBar,
  DarkBackground,
  TopRightCircle,
  ArrowBackLeft,
  SecondHeader,
  StyledText,
  StyledTab,
  MarginedInput
} from '../../style';

const Realm = require('realm');

const EditComponent = ({navigation, route}) => {
  const [wordList, setWordList] = useState([]);
  const [wordListData, setWordListData] = useState([])
  const [categoriesList, setCategoriesList] = useState([]);
  const [editedWord, setEditedWord] = useState(null);
  const [editedWordObject, setEditedWordObject] = useState(null);
  const [editedWordCategories, setEditedWordCategories] = useState([])
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  const realmToArray =(realmArray) => {
    return JSON.parse(JSON.stringify(Array.prototype.slice.call(realmArray, 0, realmArray.length))) 
  }

  useEffect(() => {
    Realm.open({schema: [CategorySchema, WordSchema]}).then(async (realm) => {
      await setIsLoading(true)
       let words = await realm.objects('Word');
        await setWordList(realmToArray(words));
        await setWordListData(realmToArray(words))
       let categories = await realm.objects('Category');
       await console.log(categories)
        await setCategoriesList(realmToArray(categories));
        await setEditedWordCategories(realmToArray(categories))

      await setIsLoading(false)
    });
  }, [selectedIndex]);

  const resetStomachNumber = () => {
    Realm.open({schema: [WordSchema, CategorySchema]}).then((realm) => {
      realm.write(() => {
        let stomachNumberWord = editedWordObject.stomachNumber
        const category = JSON.parse(JSON.stringify(realm.objects('Category').filtered(`id = "${editedWordObject.category}"`)))
        console.log('xd', stomachNumberWord, category)
        realm.create('Category', {
          id: editedWordObject.category, 
          [stomachNumberWord]: category[0][stomachNumberWord] - 1,
          1: category[0][1] + 1
        }, 'modified');
        realm.create('Word', {id: editedWordObject.id, stomachNumber: 1}, 'modified');
      });
    });
    Toast.showWithGravity(
      translate('save_element'),
      Toast.LONG,
      Toast.BOTTOM,
    );
  };

  const saveWord = () => {
    Realm.open({schema: [WordSchema, CategorySchema]}).then((realm) => {
      realm.write(() => {
        realm.create('Word', {
          id: editedWordObject.id, 
          word: editedWordObject.word, 
          translate: editedWordObject.translate,
          category: editedWordObject.category
        }, 'modified');
      });
    });
    setEditedWord(null)
    setEditedWordObject(null)
    Toast.showWithGravity(
      translate('save_element'),
      Toast.LONG,
      Toast.BOTTOM,
    );
  };

  const deleteWord = () => {
    let locWordList = [...wordList];
    _.remove(locWordList, (o) => o.id === editedWordObject.id);
    Realm.open({schema: [WordSchema]}).then((realm) => {
      realm.write(() => {
        const category = JSON.parse(JSON.stringify(realm.objects('Category').filtered(`id = "${editedWordObject.category}"`)))
        realm.create('Category', {
          id: editedWordObject.category, 
          1: category[0][1] - 1
        }, 'modified');
        realm.delete(realm.objectForPrimaryKey('Word', editedWordObject.id));
      });
    });
    setWordList(wordList.filter(o => o.id !== editedWordObject.id))
    setWordListData(wordListData.filter(o => o.id !== editedWordObject.id))
    setEditedWord(null);
    setEditedWordObject(null);
    Toast.showWithGravity(
      translate('element_deleted'),
      Toast.LONG,
      Toast.BOTTOM,
    );
  };

  const deleteCategory = (category) => {
    let locCategoriesList = [...categoriesList];
    _.remove(locCategoriesList, (o) => o.id === category.id);
    setCategoriesList(locCategoriesList);
    Realm.open({schema: [WordSchema, CategorySchema]}).then((realm) => {
      realm.write(() => {
        let words = realm
          .objects('Word')
          .filtered(`category = "${category.name}"`);
        realm.delete(words);
        realm.delete(realm.objectForPrimaryKey('Category', category.id));
      });
    });
  };

  const onSelectWord = (index) => {
    console.log(wordListData[index], index)
    setEditedWord(wordListData[index].word);
    setEditedWordObject(wordListData[index])
  };

  const filter = (item, query) =>
    item.toLowerCase().includes(query.toLowerCase());

  const onChangeTextWord = (query) => {
    setEditedWord(query)
    setWordListData(
      wordList.filter((item) => filter(item.word, query))
    );
  };

  const onChangeTextWordCategory = (query) => {
    onChangeWordObject('category', query)
    setEditedWordCategories(
      categoriesList.filter((item) => filter(item.name, query))
    );
  };

  const renderOptionWords = (item, index) => (
    <AutocompleteItem key={index} title={item.word} />
  );

  const renderOptionCategories = (item, index) => (
    <AutocompleteItem key={index} title={item.name} />
  );

  const onChangeWordObject = (field, value) => {
    const newObject = {...editedWordObject}
    newObject[field] = value
    setEditedWordObject(newObject)
  }

  return (
    <DarkBackground>
      <TopRightCircle width={200} height={100} size="giant" />
      <ArrowBackLeft onPress={() => navigation.pop()} />
      <SecondHeader category="h5" color={2} right={-80}>
        {route.params.translate('edit')}
      </SecondHeader>
      {isLoading ? <Spinner />: <View style={{height: '100%', width: '100%'}}><StyledTabBar
        selectedIndex={selectedIndex}
        style={{marginTop: 100}}
        onSelect={(index) => setSelectedIndex(index)}>
        <StyledTab
          title={
            <StyledText style={{color: !selectedIndex ? 'black' : 'white'}}>
              {route.params.translate('words')}
            </StyledText>
          }
          color={3}
        />
        <StyledTab
          title={
            <StyledText style={{color: selectedIndex ? 'black' : 'white'}}>
              {route.params.translate('categories')}
            </StyledText>
          }
          color={3}
        />
      </StyledTabBar>
      <ColorText category="h4" color={1}>{route.params.translate('word')}</ColorText>
          <MarginedAutocomplete
            placeholder={route.params.translate('search_word')}
            placement="top"
            value={editedWord}
            onSelect={onSelectWord}
            onChangeText={onChangeTextWord}>
            {wordListData.map(renderOptionWords)}
          </MarginedAutocomplete>
      {editedWordObject &&<View>
          <ColorText category="h5" color={1}>
            {route.params.translate('word')}
          </ColorText>
          <MarginedInput
            value={editedWordObject.word}
            placeholder={route.params.translate('text_word')}
            onChangeText={(nextValue) => onChangeWordObject('word', nextValue)}
          />
          <ColorText category="h5" color={1}>
            {route.params.translate('translate')}
          </ColorText>
          <MarginedInput
            value={editedWordObject.translate}
            placeholder={route.params.translate('text_word')}
            onChangeText={(nextValue) => onChangeWordObject('translate', nextValue)}
          />
          <MarginedAutocomplete
            placeholder={route.params.translate('search_category')}
            placement="top"
            value={editedWordObject.category}
            onSelect={(value) => onChangeWordObject('category', editedWordCategories[value].name)}
            onChangeText={onChangeTextWordCategory}>
            {editedWordCategories.map(renderOptionCategories)}
          </MarginedAutocomplete>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
            <Button onPress={saveWord} status="success">{route.params.translate('save')}</Button>
            <Button onPress={deleteWord} status="danger">{route.params.translate('delete')}</Button>
            <Button onPress={resetStomachNumber}>{route.params.translate('reset_progress')}</Button>
          </View>
        </View>}
    </View>}
    </DarkBackground>
  );
};

export default EditComponent;
