import React, { useState, useEffect } from 'react';
import { AutocompleteItem } from '@ui-kitten/components';
import { CategorySchema, WordSchema } from '../schemas';
import Toast from 'react-native-simple-toast';
import {
  MarginedInput,
  MarginedAutocomplete,
  ColorText,
  DarkBackground,
  TopRightCircle,
  SecondHeader,
  ArrowBackLeft,
  GreenButton,
  StyledTabBar,
  StyledTab,
  StyledText,
} from '../../style';

const Realm = require('realm');

const NewWordsComponent = ({ navigation, route }) => {
  const [newWord, setNewWord] = useState('');
  const [newWordTranslate, setNewWordTranslate] = useState('');
  const [category, setCategory] = useState('');
  const [isNewCategory, setIsNewCategory] = useState(0);
  const [categoriesList, setCategoriesList] = useState([]);
  const [categoriesListData, setCategoriesListData] = useState([]);
  useEffect(() => {
    let categoriesList;
    let wordList;
    Realm.open({ schema: [CategorySchema, WordSchema] }).then((realm) => {
      categoriesList = realm.objects('Category');
      wordList = realm.objects('Word');
      realm.write(() => {
        !categoriesList.length &&
          realm.create('Category', {
            id: 'ogólne',
            name: 'ogólne',
            isSelected: false,
            1: 1,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
          });
        !wordList.length &&
          realm.create('Word', {
            id: Math.floor(Math.random() * 5000000),
            word: 'pies',
            translate: 'dog',
            category: 'ogólne',
            stomachNumber: 1,
          });
      });
      let categories = [];
      realm.objects('Category').map((category) => {
        categories.push(category.name);
      });
      setCategoriesList(categories)
      setCategoriesListData(categories);
      realm.close();
    });
  }, []);

  const renderOption = (item, index) => (
    <AutocompleteItem key={index} title={item} />
  );

  const filter = (item, query) =>
    item.toLowerCase().includes(query.toLowerCase());

  const onSelect = (index) => {
    setCategory(categoriesListData[index]);
  };

  const onChangeText = (query) => {
    setCategory(query);
    setCategoriesListData(
      categoriesList.filter((item) => filter(item, query))
    );
  };
  const addWord = () => {
    Realm.open({ schema: [CategorySchema, WordSchema] }).then((realm) => {
      realm.write(() => {
        (isNewCategory === 0 &&
          !categoriesListData.find((el) => el === category)) ?
          realm.create('Category', {
            id: category,
            name: category,
            isSelected: false,
            1: 1,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
          }) : realm.create('Category', {id: category, 1: realm.objects('Category').filtered(`id = "${category}"`)[0]['1']+1}, 'modified');
        realm.create('Word', {
          id: Math.floor(Math.random() * 5000000),
          word: newWord,
          translate: newWordTranslate,
          category: category,
          stomachNumber: 1,
        });
      });

      realm.close();
    });
  };
  return (
    <DarkBackground>
      <TopRightCircle width={200} height={100} size="giant" />
      <ArrowBackLeft onPress={() => navigation.pop()} />
      <SecondHeader category="h5" color={2} right={-80}>
        {route.params.translate('new')}
      </SecondHeader>
      <ColorText category="h5" color={1}>
        {route.params.translate('word_sentence')}
      </ColorText>
      <MarginedInput
        value={newWord}
        placeholder={route.params.translate('text_word')}
        onChangeText={(nextValue) => setNewWord(nextValue)}
      />
      <ColorText category="h3" color={1}>
        {route.params.translate('translate')}
      </ColorText>
      <MarginedInput
        value={newWordTranslate}
        placeholder={route.params.translate('text_translate')}
        onChangeText={(nextValue) => setNewWordTranslate(nextValue)}
      />
      <StyledTabBar
        selectedIndex={isNewCategory}
        onSelect={(index) => setIsNewCategory(index)}
      >
        <StyledTab
          color={3}
          title={
            <StyledText style={{ color: !isNewCategory ? 'black' : 'white' }}>
              {route.params.translate('new_category')}
            </StyledText>
          }
        />
        <StyledTab
          color={3}
          title={
            <StyledText style={{ color: isNewCategory ? 'black' : 'white' }}>
              {route.params.translate('choose_category')}
            </StyledText>
          }
        />
      </StyledTabBar>
      {!isNewCategory ? (
        <>
          <MarginedInput
            value={category}
            placeholder={route.params.translate('text_category')}
            onChangeText={(nextValue) => setCategory(nextValue)}
          />
        </>
      ) : (
        <>
          <MarginedAutocomplete
            placeholder={route.params.translate('search_category')}
            placement="top"
            value={category}
            onSelect={onSelect}
            onChangeText={onChangeText}
          >
            {categoriesListData.map(renderOption)}
          </MarginedAutocomplete>
        </>
      )}
      <GreenButton
        disabled={
          !newWord.length || !newWordTranslate.length || !category.length
        }
        onPress={() => {
          addWord();
          Toast.showWithGravity(
            route.params.translate('created_new_word'),
            Toast.LONG,
            Toast.BOTTOM
          );
          navigation.pop();
        }}
      >
        <StyledText>{route.params.translate('add_word')}</StyledText>
      </GreenButton>
    </DarkBackground>
  );
};

export default NewWordsComponent;
