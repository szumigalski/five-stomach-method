import React from 'react';
import {
  DarkBackground,
  TopRightCircle,
  ArrowBackLeft,
  SecondHeader,
  ColorText,
} from '../../style';
import { ScrollView } from 'react-native';

const InstructionComponent = ({ navigation, route }) => {
  return (
    <DarkBackground>
      <ArrowBackLeft onPress={() => navigation.pop()} />
      <TopRightCircle width={250} height={100} />
      <SecondHeader color={2} category="h5" right={0}>
        {route.params.translate('instruction')}
      </SecondHeader>
      <ScrollView style={{ marginTop: 100 }}>
        <ColorText color={1} category="h5">
          {route.params.translate('adding_words')}
        </ColorText>
        <ColorText color={1}>
          {route.params.translate('adding_words_desc')}
        </ColorText>
        <ColorText color={1} category="h5" style={{ marginTop: 15 }}>
        {route.params.translate('learning')}
        </ColorText>
        <ColorText color={1}>
        {route.params.translate('learning_desc')}
        </ColorText>
        <ColorText color={1}>
        {route.params.translate('learning_desc_2')}
        </ColorText>
        <ColorText color={1} category="h5" style={{ marginTop: 15 }}>
        {route.params.translate('edition')}
        </ColorText>
        <ColorText color={1}>
        {route.params.translate('edition_desc')}
        </ColorText>
        <ColorText color={1} category="h5" style={{ marginTop: 15 }}>
        {route.params.translate('test_words_and_categories')}
        </ColorText>
        <ColorText color={1}>
        {route.params.translate('test_desc')}
        </ColorText>
      </ScrollView>
    </DarkBackground>
  );
};

export default InstructionComponent;
