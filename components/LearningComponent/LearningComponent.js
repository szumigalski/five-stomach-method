import React, {useState, useEffect} from 'react';
import {CheckBox, Spinner} from '@ui-kitten/components';
import {FlatList, View} from 'react-native';
import {WordSchema, CategorySchema} from '../schemas';
import {
  GreenButton,
  LearningLayout,
  ColorText,
  ListItemCheckbox,
  TopRightCircle,
  SecondHeader,
  ArrowBackLeft,
  StyledText,
} from '../../style';
const Realm = require('realm');

const LearningComponent = ({navigation, route}) => {
  const [catCheckboxes, setCatCheckboxes] = useState([]);
  const [categoriesList, setCategoriesList] = useState([]);
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    Realm.open({schema: [CategorySchema]}).then(async (realm) => {
      let categories = await realm.objects('Category');
      let checkboxes = await [];
      let locCategories = await [];
      await categories.map((category) => {
        checkboxes.push({ 
          id: category.id, 
          selected: category.isSelected,
          1: category[1],
          2: category[2],
          3: category[3],
          4: category[4],
          5: category[5],
          6: category[6]
        });
        locCategories.push(category.name);
      });
      await setCatCheckboxes(checkboxes);
      await setCategoriesList(locCategories);
      await realm.close();
      await setIsLoading(false)
    });
  }, []);

  let onCheckboxChange = (id, nextChecked) => {
    Realm.open({schema: [CategorySchema]}).then((realm) => {
      realm.write(() => {
        realm.create(
          'Category',
          {
            id: catCheckboxes[id].id,
            isSelected: nextChecked,
          },
          'modified',
        );
      })
    });
    let change = [...catCheckboxes];
    change[id].selected = nextChecked;
    setCatCheckboxes(change);
  };

  const renderItem = ({item, index}) => {
    return (
      <ListItemCheckbox key={index} isSelected={catCheckboxes[index].selected}>
        <CheckBox
          checked={catCheckboxes[index].selected}
          onChange={(nextChecked) => onCheckboxChange(index, nextChecked)}
        />
        <View>
          <ColorText color={2}>{item}</ColorText>
          <ColorText 
            color={2}
            style={{fontSize: 12}}
          >{catCheckboxes[index][1]+ ' / '+catCheckboxes[index][2]+' / '+catCheckboxes[index][3]+' / '+catCheckboxes[index][4]+' / '+catCheckboxes[index][5]+' / '+catCheckboxes[index][6]}</ColorText>
        </View>
      </ListItemCheckbox>
    );
  };
  return (
    <LearningLayout>
      <ArrowBackLeft onPress={() => navigation.pop()} />
      <TopRightCircle width={250} height={150} />
      <SecondHeader color={2} category="h4" right={0}>
        {route.params.translate('choose_category')}
      </SecondHeader>
      {isLoading 
      ? <View style={{ marginTop: 150 }}>
          <Spinner size='giant' status='basic' />
        </View>
      : 
      <>
        <FlatList
          data={categoriesList}
          style={{marginTop: 150, marginBottom: 50}}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
        <GreenButton
          onPress={() => {
            navigation.navigate('Nauka', { translate: (word) => route.params.translate(word) });
          }}>
          <StyledText>{route.params.translate('start_learning')}</StyledText>
        </GreenButton>
      </>}
    </LearningLayout>
  );
};

export default LearningComponent;
