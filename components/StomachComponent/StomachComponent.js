import React, {useState, useEffect} from 'react';
import {Button, Icon, Spinner} from '@ui-kitten/components';
import {View} from 'react-native';
import {CategorySchema, WordSchema} from '../schemas';

const Realm = require('realm');
import {
  ArrowBackLeft,
  TopRightCircle,
  WordItem,
  DarkBackground,
  ColorText,
  SecondHeader,
  ColoredButton,
  StyledText,
  RowForButtons,
} from '../../style';
import { changeStomachNumberCategory } from '../../addons'

const StomachComponent = ({navigation, route}) => {
  const [wordNumber, setWordNumber] = useState(0);
  const [isAnswered, setIsAnswered] = useState(false);
  const [stomachNumber, setStomachNumber] = useState(1);
  const [wordList, setWordList] = useState([]);
  const [isSwiched, setIsSwitched] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  let stomachNames = [
    route.params.translate('first'), 
    route.params.translate('second'), 
    route.params.translate('third'), 
    route.params.translate('fourth'), 
    route.params.translate('fifth')
  ];

  useEffect(() => {
    Realm.open({schema: [WordSchema, CategorySchema]}).then(async (realm) => {
      let namesCategories = await []
      let filterstring = await ''
      let selectedCategories = await realm
        .objects('Category')
        .filtered(`isSelected = true`);
      await selectedCategories.map((category, i) => {
        i === 0 
          ? (filterstring += `category = "${category.name}"`) 
          : (filterstring += ` || category = "${category.name}"`)
        namesCategories.push(category.name)
      })
      let words = await realm
        .objects('Word')
        .filtered(`stomachNumber = ${stomachNumber}`)
        .filtered(`${filterstring}`);
      await setIsLoading(false)
      await setWordList(words);
    });
  }, [isAnswered, stomachNumber]);

  const RefreshIcon = (props) => <Icon {...props} name="sync-outline" />;

  return (
    <DarkBackground style={{paddingBottom: 150}}>
      <TopRightCircle width={200} height={100} size="giant" />
      <ArrowBackLeft onPress={() => navigation.pop()} />
      <SecondHeader category="h5" color={2} right={-50}>
        {route.params.translate('learning')}
      </SecondHeader>
      {isLoading 
      ? 
      <Spinner size='giant' status='basic' />
      : 
      <>
        <ColorText category="h3" color={1}>{`${
          stomachNames[stomachNumber - 1]
        } ${route.params.translate('stomach')}`}</ColorText>
        <WordItem category="h4">
          {wordList && wordList.length && wordNumber < wordList.length
            ? isAnswered
              ? wordList[wordNumber][isSwiched ? 'translate' : 'word']
              : wordList[wordNumber][isSwiched ? 'word' : 'translate']
            : route.params.translate('no_words')}
        </WordItem>
        {isAnswered ? (
          <View style={{flexDirection: 'row', margin: 20}}>
            <Button
              status="success"
              style={{marginRight: 20}}
              onPress={() => {
                changeStomachNumberCategory(stomachNumber, stomachNumber + 1, wordList[wordNumber].category )
                Realm.open({schema: [WordSchema]}).then((realm) => {
                  realm.write(() => {
                    realm.create(
                      'Word',
                      {
                        id: wordList[wordNumber].id,
                        stomachNumber: stomachNumber + 1,
                      },
                      'modified',
                    );
                  });
                });
                setIsAnswered(false);
              }}>
              {route.params.translate('good')}
            </Button>
            <Button
              status="danger"
              onPress={() => {
                stomachNumber > 1 && changeStomachNumberCategory(stomachNumber, 1, wordList[wordNumber].category )
                Realm.open({schema: [WordSchema]}).then((realm) => {
                  realm.write(() => {
                    realm.create(
                      'Word',
                      {
                        id: wordList[wordNumber].id,
                        stomachNumber: 1,
                      },
                      'modified',
                    );
                  });
                });
                setIsAnswered(false);
                stomachNumber === 1 && setWordNumber(wordNumber + 1);
              }}>
              {route.params.translate('bad')}
            </Button>
          </View>
        ) : (
          <View style={{flexDirection: 'row', margin: 20}}>
            <ColoredButton
              disabled={wordNumber >= wordList.length}
              color={3}
              onPress={() => {
                setIsAnswered(true);
              }}>
              <StyledText>{route.params.translate('check')}</StyledText>
            </ColoredButton>
          </View>
        )}
        <RowForButtons marginBottom={70}>
          {stomachNumber < 5 && (
            <ColoredButton
              color={3}
              style={{width: '45%'}}
              onPress={() => {
                setStomachNumber(stomachNumber + 1);
                setWordNumber(0);
              }}>
              <StyledText>{route.params.translate('next_stomach')}</StyledText>
            </ColoredButton>
          )}
          {stomachNumber > 1 && (
            <ColoredButton
              color={3}
              style={{width: '45%'}}
              onPress={() => {
                setStomachNumber(stomachNumber - 1);
                setWordNumber(0);
              }}>
              <StyledText>{route.params.translate('previous_stomach')}</StyledText>
            </ColoredButton>
          )}
        </RowForButtons>
        <RowForButtons marginBottom={15}>
          <ColoredButton
            onPress={() => setWordNumber(0)}
            disabled={!wordNumber}
            color={1}>
            <StyledText style={{color: !wordNumber ? 'black' : 'grey'}}>
              {route.params.translate('begin')}
            </StyledText>
          </ColoredButton>
          <ColoredButton
            onPress={() => setIsSwitched(!isSwiched)}
            color={2}
            style={{height: 30, margin: 2}}
            accessoryLeft={RefreshIcon}
          />
        </RowForButtons>
      </>}
    </DarkBackground>
  );
};

export default StomachComponent;
