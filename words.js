export const categories = [
    'Family',
    'House',
    'Things in house',
    'Kitchen things',
    'Hygiene',
    'What you have',
    'Routine',
    'Childhood',
    'Wedding',
    'Die',
    'Sleep',
    'School',
    'Body parts',
    'Feelings',
    'Feelings - extended',
    'Looking',
    'Intelligence',
    'Addictions'
]

export const words = [
    [
    {
        word: 'adoption',
        translate: 'adopcja'
    },
    {
        word: 'ancestor',
        translate: 'przodek'
    },
    {
        word: 'aunt',
        translate: 'ciocia'
    },
    {
        word: 'brother',
        translate: 'brat'
    },
    {
        word: 'brother-in-law',
        translate: 'szwagier'
    },
    {
        word: 'child',
        translate: 'dziecko'
    },
    {
        word: 'cousin',
        translate: 'kuzyn'
    },
    {
        word: 'daughter',
        translate: 'córka'
    },
    {
        word: 'daughter-in-law',
        translate: 'synowa'
    },
    {
        word: 'descendant',
        translate: 'potomek'
    },
    {
        word: 'divorce',
        translate: 'rozwód'
    },
    {
        word: 'elder sister',
        translate: 'starsza siostra'
    },
    {
        word: 'extended family',
        translate: 'dalsza rodzina'
    },
    {
        word: 'family life',
        translate: 'zycie rodzinne'
    },
    {
        word: 'family reunion',
        translate: 'spotkanie rodzinne'
    },
    {
        word: 'family',
        translate: 'rodzina'
    },
    {
        word: 'father-in-law',
        translate: 'teść'
    },
    {
        word: 'foster family',
        translate: 'rodzina zastępcza'
    },
    {
        word: 'foster-child',
        translate: 'przybrane dziecko'
    },
    {
        word: 'genration',
        translate: 'pokolenie'
    },
    {
        word: 'grandchildren',
        translate: 'wnuki'
    },
    {
        word: 'grandfather',
        translate: 'dziadek'
    },
    {
        word: 'grandmother',
        translate: 'babcia'
    },
    {
        word: 'grandson',
        translate: 'wnuk'
    },
    {
        word: 'half-brother',
        translate: 'brat przyrodni'
    },
    {
        word: 'half-sister',
        translate: 'siostra przyrodnia'
    },
    {
        word: 'huband',
        translate: 'mąz'
    },
    {
        word: 'to live in separation',
        translate: 'zyć w separacji'
    },
    {
        word: 'marriage',
        translate: 'małzeństwo'
    },
    {
        word: 'married',
        translate: 'zamęzna'
    },
    {
        word: 'to marry someone',
        translate: 'poślubić kogoś'
    },
    {
        word: 'mother',
        translate: 'mama'
    },
    {
        word: 'mother-in-law',
        translate: 'teściowa'
    },
    {
        word: 'multi-generation family',
        translate: 'siostrzeniec'
    },
    {
        word: 'nephew',
        translate: 'siostrzeniec'
    },
    {
        word: 'next of kin',
        translate: 'najblizszy krewny'
    },
    {
        word: 'niece',
        translate: 'siostrzenica'
    },
    {
        word: 'numerous family',
        translate: 'rodzina wielodzietna'
    },
    {
        word: 'only child',
        translate: 'jedynak'
    },
    {
        word: 'orphan',
        translate: 'sierota'
    },
    {
        word: 'parents',
        translate: 'rodzice'
    },
    {
        word: 'to raise a family',
        translate: 'załozyć rodzinę'
    },
    {
        word: 'relatives',
        translate: 'krewni'
    },
    {
        word: 'sister',
        translate: 'siostra'
    },
    {
        word: 'sister-in-law',
        translate: 'bratowa'
    },
    {
        word: 'son',
        translate: 'syn'
    },
    {
        word: 'son-in-law',
        translate: 'zięć'
    },
    {
        word: 'step-father',
        translate: 'ojczym'
    },
    {
        word: 'step-mother',
        translate: 'macocha'
    },
    {
        word: 'twin',
        translate: 'bliźniak'
    },
    {
        word: 'uncle',
        translate: 'wujek'
    },
    {
        word: 'upbringing',
        translate: 'wychowanie'
    },
    {
        word: 'widow',
        translate: 'wdowa'
    },
    {
        word: 'widower',
        translate: 'wdowiec'
    },
    {
        word: 'wife',
        translate: 'zona'
    }
],[
    {
        word: 'accomodation',
        translate: 'zakwaterowanie'
    },
    {
        word: 'attic',
        translate: 'strych'
    },
    {
        word: 'balcony',
        translate: 'balkon'
    },
    {
        word: 'bathroom',
        translate: 'łazienka'
    },
    {
        word: 'bedroom',
        translate: 'sypialnia'
    },
    {
        word: 'ceiling',
        translate: 'sufit'
    },
    {
        word: 'cellar',
        translate: 'piwnica'
    },
    {
        word: 'central heating',
        translate: 'centralne ogrzewanie'
    },
    {
        word: 'chimney',
        translate: 'komin'
    },
    {
        word: 'corridor',
        translate: 'korytarz'
    },
    {
        word: 'dining room',
        translate: 'jadalnia'
    },
    {
        word: 'door',
        translate: 'drzwi'
    },
    {
        word: 'door mat',
        translate: 'wycieraczka'
    },
    {
        word: 'doorbell',
        translate: 'dzwonek do drzwi'
    },
    {
        word: 'family house',
        translate: 'dom jednorodzinny'
    },
    {
        word: 'fireplace',
        translate: 'kominek'
    },
    {
        word: 'flat/apartment',
        translate: 'mieszkanie'
    },
    {
        word: 'garret',
        translate: 'poddasze'
    },
    {
        word: 'gate',
        translate: 'furtka'
    },
    {
        word: 'hall',
        translate: 'przedpokój'
    },
    {
        word: 'hedge',
        translate: 'zywopłot'
    },
    {
        word: 'house',
        translate: 'dom'
    },
    {
        word: 'house owner',
        translate: 'właściciel domu'
    },
    {
        word: 'house telephone',
        translate: 'domofon'
    },
    {
        word: 'kitchen',
        translate: 'kuchnia'
    },
    {
        word: 'letterbox',
        translate: 'skrzynka na listy'
    },
    {
        word: 'lighting',
        translate: 'oświetlenie'
    },
    {
        word: 'lightning rod',
        translate: 'piorunochron'
    },
    {
        word: 'living room',
        translate: 'pokój dzienny'
    },
    {
        word: 'mortgage',
        translate: 'hipoteka'
    },
    {
        word: 'to move in',
        translate: 'wprowadzić się'
    },
    {
        word: 'to move out',
        translate: 'wyprowadzić się'
    },
    {
        word: 'real estate',
        translate: 'nieruchomość'
    },
    {
        word: 'rent',
        translate: 'czynsz'
    },
    {
        word: 'roof',
        translate: 'dach'
    },
    {
        word: 'room',
        translate: 'pokój'
    },
    {
        word: 'running water',
        translate: 'bieząca woda'
    },
    {
        word: 'semi-detached house',
        translate: 'dom bliźniak'
    },
    {
        word: 'sewage sytem',
        translate: 'kanalizacja'
    },
    {
        word: 'suite',
        translate: 'apartament'
    },
    {
        word: 'tenement',
        translate: 'czynszowy'
    },
    {
        word: 'threshold',
        translate: 'próg'
    },
    {
        word: 'toilet',
        translate: 'toaleta'
    },
    {
        word: 'view finder',
        translate: 'wizjer'
    },
    {
        word: 'wainscot',
        translate: 'boazeria'
    },
    {
        word: 'wall',
        translate: 'ściana'
    },
    {
        word: 'window',
        translate: 'okno'
    },
    {
        word: 'window sill',
        translate: 'parapet'
    }
], [
    {
        word: 'alarm clock',
        translate: 'budzik'
    },
    {
        word: 'armchair',
        translate: 'fotel'
    },
    {
        word: 'bed',
        translate: 'łózko'
    },
    {
        word: 'bunk beds',
        translate: 'łózko piętrowe'
    },
    {
        word: 'carpet',
        translate: 'dywan'
    },
    {
        word: 'chair',
        translate: 'krzesło'
    },
    {
        word: 'container',
        translate: 'pojemnik'
    },
    {
        word: 'cup',
        translate: 'filizanka'
    },
    {
        word: 'curtail',
        translate: 'zasłona'
    },
    {
        word: 'cutlery',
        translate: 'sztućce'
    },
    {
        word: 'dishwasher',
        translate: 'zmywarka'
    },
    {
        word: 'drawer',
        translate: 'szuflada'
    },
    {
        word: 'floor',
        translate: 'podłoga'
    },
    {
        word: 'fork',
        translate: 'widelec'
    },
    {
        word: 'freezer',
        translate: 'zamrazarka'
    },
    {
        word: 'furniture',
        translate: 'meble'
    },
    {
        word: 'glass',
        translate: 'szklanka'
    },
    {
        word: 'heater',
        translate: 'grzejnik'
    },
    {
        word: 'knife',
        translate: 'nóz'
    },
    {
        word: 'lamp',
        translate: 'lampa'
    },
    {
        word: 'lock',
        translate: 'zamek'
    },
    {
        word: 'mirror',
        translate: 'lustro'
    },
    {
        word: 'mug',
        translate: 'kubek'
    },
    {
        word: 'oven',
        translate: 'piekarnik'
    },
    {
        word: 'pan',
        translate: 'patelnia'
    },
    {
        word: 'piece of furniture',
        translate: 'mebel'
    },
    {
        word: 'radiator',
        translate: 'kaloryfer'
    },
    {
        word: 'sheet',
        translate: 'prześcieradło'
    },
    {
        word: 'sink',
        translate: 'zlewozmywak'
    },
    {
        word: 'soap',
        translate: 'mydło'
    },
    {
        word: 'sofa',
        translate: 'sofa/kanapa'
    },
    {
        word: 'spoon',
        translate: 'łyzka'
    },
    {
        word: 'table',
        translate: 'stół'
    },
    {
        word: 'tablecloth',
        translate: 'obrus'
    },
    {
        word: 'tableware',
        translate: 'zastawa stołowa'
    },
    {
        word: 'torch',
        translate: 'latarka'
    },
    {
        word: 'wardrobe',
        translate: 'szafa'
    },
    {
        word: 'washbasin',
        translate: 'umywalka'
    },
    {
        word: 'washing machine',
        translate: 'pralka'
    }
], [
    {
        word: 'biscuit sheet',
        translate: 'blacha do ciastek'
    },
    {
        word: 'bootle opener',
        translate: 'otwieracz do butelek'
    },
    {
        word: 'butter curler',
        translate: 'nóz do masła'
    },
    {
        word: 'cake tin',
        translate: 'forma do pieczenia'
    },
    {
        word: 'cheese knife',
        translate: 'nóz do sera'
    },
    {
        word: 'cleaver',
        translate: 'tasak'
    },
    {
        word: 'colander',
        translate: 'durszlak'
    },
    {
        word: 'corkscrew',
        translate: 'korkociąg'
    },
    {
        word: 'cup',
        translate: 'filizanka'
    },
    {
        word: 'cutlery',
        translate: 'sztućce'
    },
    {
        word: 'dessert fork',
        translate: 'widelec deserowy'
    },
    {
        word: 'dinner fork',
        translate: 'widelec obiadowy'
    },
    {
        word: 'draining spoon',
        translate: 'lyzka do odsączania'
    },
    {
        word: 'egg timer',
        translate: 'minutnik do jajek'
    },
    {
        word: 'fish fork',
        translate: 'widelec do ryb'
    },
    {
        word: 'funnel',
        translate: 'lejek'
    },
    {
        word: 'garlic press',
        translate: 'smok do czosnku'
    },
    {
        word: 'grater',
        translate: 'tarka'
    },
    {
        word: 'kitchen timer',
        translate: 'minutnik kuchenny'
    },
    {
        word: 'kitchen untensils',
        translate: 'przybory kuchenne'
    },
    {
        word: 'knife',
        translate: 'nóz'
    },
    {
        word: 'ladle',
        translate: 'chochla'
    },
    {
        word: 'measuring spoon',
        translate: 'łyzka do odmierzania'
    },
    {
        word: 'mincer',
        translate: 'maszynka do mięsa'
    },
    {
        word: 'nuttracker',
        translate: 'dziadek do orzechów'
    },
    {
        word: 'oyster fork',
        translate: 'widelec do ostryg'
    },
    {
        word: 'pasta maker',
        translate: 'maszynka do makaronu'
    },
    {
        word: 'pastry cutting wheel',
        translate: 'radełko'
    },
    {
        word: 'peeler',
        translate: 'obieraczka'
    },
    {
        word: 'pepperpot',
        translate: 'pieprzniczka'
    },
    {
        word: 'plate',
        translate: 'talerz'
    },
    {
        word: 'potato masher',
        translate: 'ugniatacz do ziemniaków'
    },
    {
        word: 'rolling pin',
        translate: 'wałek'
    },
    {
        word: 'salad fork',
        translate: 'widelec do sałatek'
    },
    {
        word: 'salad spinner',
        translate: 'wirówka do sałaty'
    },
    {
        word: 'salt cellar',
        translate: 'solniczka'
    },
    {
        word: 'saucer',
        translate: 'spodek'
    },
    {
        word: 'sifter',
        translate: 'przesiewacz'
    },
    {
        word: 'silverware',
        translate: 'zastawa stołowa'
    },
    {
        word: 'skimmer',
        translate: 'cedzidło'
    },
    {
        word: 'spoon',
        translate: 'łyzka'
    },
    {
        word: 'strainer',
        translate: 'sitko'
    },
    {
        word: 'sugar basin',
        translate: 'cukiernica'
    },
    {
        word: 'tablespoon',
        translate: 'łyzka stołowa'
    },
    {
        word: 'tea infuser',
        translate: 'zaparzacz do herbaty'
    },
    {
        word: 'teaspoon',
        translate: 'łyzeczka do herbaty'
    },
    {
        word: 'tin opener',
        translate: 'otwieracz do konserw'
    },
    {
        word: 'tray',
        translate: 'taca/tacka'
    },
    {
        word: 'whisk',
        translate: 'trzepaczka'
    },
    {
        word: 'zester',
        translate: 'skrobak do cytrusów'
    },
], [
    {
        word: 'bidet',
        translate: 'bidet'
    },
    {
        word: 'black-head',
        translate: 'wągier'
    },
    {
        word: 'to brush teeth',
        translate: 'myć zęby'
    },
    {
        word: 'clean',
        translate: 'czyścić/czysty'
    },
    {
        word: 'to comb hair',
        translate: 'czesać włosy'
    },
    {
        word: 'dirt',
        translate: 'brud'
    },
    {
        word: 'dirty',
        translate: 'brudny'
    },
    {
        word: 'to file mails',
        translate: 'piłować paznokcie'
    },
    {
        word: 'to flush the toilet',
        translate: 'spuścić wodę'
    },
    {
        word: 'to have a shower',
        translate: 'brać prysznic'
    },
    {
        word: 'hygiene',
        translate: 'higiena'
    },
    {
        word: 'hygienic',
        translate: 'higieniczny'
    },
    {
        word: 'lavatory seat',
        translate: 'sedes'
    },
    {
        word: 'to rinse hair',
        translate: 'płukac włosy'
    },
    {
        word: 'to rinse mouth',
        translate: 'płukać usta'
    },
    {
        word: 'shaving',
        translate: 'golenie'
    },
    {
        word: 'soap dish',
        translate: 'mydelniczka'
    },
    {
        word: 'soap',
        translate: 'mydło'
    },
    {
        word: 'sponge',
        translate: 'gąbka'
    },
    {
        word: 'toilet',
        translate: 'toaleta'
    },
    {
        word: 'toothbrush',
        translate: 'szczoteczka do zębów'
    },
    {
        word: 'toothpaste',
        translate: 'pasta do zębów'
    },
    {
        word: 'towel',
        translate: 'ręcznik'
    },
    {
        word: 'urinal',
        translate: 'pisuar'
    },
    {
        word: 'to wash hair',
        translate: 'myć włosy'
    },
    {
        word: 'to wash',
        translate: 'myć się'
    },
    {
        word: 'washbasin',
        translate: 'umywalka'
    }
], [
    {
        word: 'bag',
        translate: 'plecak/saszetka'
    },
    {
        word: 'box of matches',
        translate: 'pudełko zapałek'
    },
    {
        word: 'bracelet',
        translate: 'bransoleta'
    },
    {
        word: 'briefcase',
        translate: 'aktówka'
    },
    {
        word: 'calendar',
        translate: 'kalendarz'
    },
    {
        word: 'cap',
        translate: 'czapka'
    },
    {
        word: 'car keys',
        translate: 'kluczyki do auta'
    },
    {
        word: 'chain',
        translate: 'łańcuszek'
    },
    {
        word: 'chewing gum',
        translate: 'guma do zucia'
    },
    {
        word: 'coin',
        translate: 'moneta'
    },
    {
        word: 'comb',
        translate: 'grzebień'
    },
    {
        word: 'contact lens',
        translate: 'szkła kontaktowe'
    },
    {
        word: 'credit card',
        translate: 'karta kredytowa'
    },
    {
        word: 'dental thread',
        translate: 'deodorant'
    },
    {
        word: 'diary',
        translate: 'terminarz'
    },
    {
        word: 'earring',
        translate: 'kolczyk'
    },
    {
        word: 'fountain pen',
        translate: 'pióro wieczne'
    },
    {
        word: 'glasses',
        translate: 'okólary'
    },
    {
        word: 'handkerchief',
        translate: 'chusteczka do nosa'
    },
    {
        word: 'hat',
        translate: 'kapelusz'
    },
    {
        word: 'jewel',
        translate: 'klejnot'
    },
    {
        word: 'jewellery',
        translate: 'bizuteria'
    },
    {
        word: 'key',
        translate: 'klucz'
    },
    {
        word: 'laptop',
        translate: 'laptop'
    },
    {
        word: 'lighter',
        translate: 'zapalniczka'
    },
    {
        word: 'lipstick',
        translate: 'szminka'
    },
    {
        word: 'locket',
        translate: 'medalionik'
    },
    {
        word: 'mobile phone',
        translate: 'telefon komórkowy'
    },
    {
        word: 'money',
        translate: 'pieniądze'
    },
    {
        word: 'nail file',
        translate: 'pilnik do paznokci'
    },
    {
        word: 'nail scissors',
        translate: 'nozyczki do paznokci'
    },
    {
        word: 'necklace',
        translate: 'naszjnik'
    },
    {
        word: 'passport',
        translate: 'paszport'
    },
    {
        word: 'pen',
        translate: 'pióro'
    },
    {
        word: 'pencil',
        translate: 'ołówek'
    },
    {
        word: 'pendant',
        translate: 'wisiorek'
    },
    {
        word: 'phone card',
        translate: 'karta telefoniczna'
    },
    {
        word: 'powder puff',
        translate: 'puderniczka'
    },
    {
        word: 'purse',
        translate: 'portmonetka'
    },
    {
        word: 'razor',
        translate: 'brzytwa'
    },
    {
        word: 'ring',
        translate: 'pierścionek'
    },
    {
        word: 'scarf',
        translate: 'szalik'
    },
    {
        word: 'scissors',
        translate: 'nozyczki'
    },
    {
        word: 'shampoo',
        translate: 'szampon'
    },
    {
        word: 'soap',
        translate: 'mydło'
    },
    {
        word: 'sunglasses',
        translate: 'okulary przeciwsłoneczne'
    },
    {
        word: 'toilet paper',
        translate: 'papier toaletowy'
    },
    {
        word: 'toothpaste',
        translate: 'pasta do zębów'
    },
    {
        word: 'toothpick',
        translate: 'wykałaczka'
    },
    {
        word: 'towel',
        translate: 'ręcznik'
    },
    {
        word: 'underwear',
        translate: 'bielizna'
    },
    {
        word: 'vanity case',
        translate: 'kosmetyczka'
    },
    {
        word: 'wallet',
        translate: 'portfel'
    },
    {
        word: 'watch',
        translate: 'zegarek'
    },
    {
        word: 'wedding ring',
        translate: 'obrączka'
    }
], [
    {
        word: 'to be in a hurry',
        translate: 'spieszyć się'
    },
    {
        word: 'to clean',
        translate: 'czyścić'
    },
    {
        word: 'to comb',
        translate: 'czesać'
    },
    {
        word: 'to cook dinner',
        translate: 'gotować obiad'
    },
    {
        word: 'to cook',
        translate: 'gotować'
    },
    {
        word: 'to do exercises',
        translate: 'gimnastykować się'
    },
    {
        word: 'to drink',
        translate: 'pić'
    },
    {
        word: 'to dry',
        translate: 'suszyć'
    },
    {
        word: 'to dust',
        translate: 'ścierać kurze'
    },
    {
        word: 'to eat',
        translate: 'jeść'
    },
    {
        word: 'to empty the bin',
        translate: 'wyrzucić śmieci'
    },
    {
        word: 'to exercise',
        translate: 'ćwiczyć'
    },
    {
        word: 'to fall asleep',
        translate: 'zasypiać'
    },
    {
        word: 'to fall into routine',
        translate: 'wpaść w rutynę'
    },
    {
        word: 'to get dressed',
        translate: 'ubierać się'
    },
    {
        word: 'to go jogging',
        translate: 'iść pobiegać'
    },
    {
        word: 'to go shopping',
        translate: 'iść na zakupy'
    },
    {
        word: 'to gossip',
        translate: 'plotkować'
    },
    {
        word: 'to have a bath',
        translate: 'kąpać się'
    },
    {
        word: 'to help',
        translate: 'pomagać'
    },
    {
        word: 'to hoover',
        translate: 'odkurzać'
    },
    {
        word: 'to iron',
        translate: 'prasować'
    },
    {
        word: 'to listen a music',
        translate: 'słuchać muzyki'
    },
    {
        word: 'to make a bed',
        translate: 'ścielić łózko'
    },
    {
        word: 'to meet',
        translate: 'spotkać'
    },
    {
        word: 'nap',
        translate: 'drzemka'
    },
    {
        word: 'to read a book',
        translate: 'czytać ksiązkę'
    },
    {
        word: 'relaxation',
        translate: 'relaks'
    },
    {
        word: 'rest',
        translate: 'odpoczynek'
    },
    {
        word: 'routine',
        translate: 'rutyna'
    },
    {
        word: 'to shave',
        translate: 'golić się'
    },
    {
        word: 'to sleep',
        translate: 'spać'
    },
    {
        word: 'to sweep',
        translate: 'zamiatać'
    },
    {
        word: 'to take a shower',
        translate: 'brać prysznic'
    },
    {
        word: 'to undress',
        translate: 'rozebrać się'
    },
    {
        word: 'to wait for a bus',
        translate: 'czekać na autobus'
    },
    {
        word: 'to wake up',
        translate: 'budzić się'
    },
    {
        word: 'to walk',
        translate: 'chodzić/spacerować'
    },
    {
        word: 'to walk the dog',
        translate: 'wyprowadzać psa'
    },
    {
        word: 'to wash',
        translate: 'prać'
    },
    {
        word: 'to wah up the dishes',
        translate: 'zmywać naczynia'
    },
    {
        word: 'to watch TV',
        translate: 'oglądać telewizję'
    },
    {
        word: 'to write',
        translate: 'pisać'
    },
], [
    {
        word: 'baby',
        translate: 'dziecko'
    },
    {
        word: 'baby boy',
        translate: 'chłopczyk'
    },
    {
        word: 'baby carriage',
        translate: 'wózek dziecięcy'
    },
    {
        word: 'baby girl',
        translate: 'dzieczynka'
    },
    {
        word: 'babyhood',
        translate: 'okres niemowlęcy'
    },
    {
        word: 'babyish',
        translate: 'dziecinny'
    },
    {
        word: 'bib',
        translate: 'śliniaczek'
    },
    {
        word: 'to breast-feed',
        translate: 'karmić piersią'
    },
    {
        word: 'Ceasarean section',
        translate: 'cesarskie cięcie'
    },
    {
        word: 'to change a baby',
        translate: 'przewijać niemowlę'
    },
    {
        word: 'child',
        translate: 'dziecko'
    },
    {
        word: 'child-mind',
        translate: 'opieka nad dzieckiem'
    },
    {
        word: 'childish',
        translate: 'dziecinny'
    },
    {
        word: 'children',
        translate: 'dzieci'
    },
    {
        word: 'cradle',
        translate: 'kołyska'
    },
    {
        word: 'don`t be a baby',
        translate: 'nie bądź dzieckiem'
    },
    {
        word: 'to dribble',
        translate: 'ślinić się'
    },
    {
        word: 'dummy',
        translate: 'smoczek'
    },
    {
        word: 'expectant mother',
        translate: 'przyszła matka'
    },
    {
        word: 'feeding bootle',
        translate: 'butelka dla niemowląt'
    },
    {
        word: 'to get pregnant',
        translate: 'zajść w ciązę'
    },
    {
        word: 'infancy',
        translate: 'niemowlęctwo'
    },
    {
        word: 'infant',
        translate: 'niemowlę'
    },
    {
        word: 'labour pains',
        translate: 'bóle porodowe'
    },
    {
        word: 'layette',
        translate: 'wyprawka niemowlęca'
    },
    {
        word: 'lullaby',
        translate: 'kołysanka'
    },
    {
        word: 'maternal',
        translate: 'macierzyński'
    },
    {
        word: 'maternity',
        translate: 'macierzyństwo'
    },
    {
        word: 'maternity dress',
        translate: 'suknia ciązowa'
    },
    {
        word: 'maternity hospital',
        translate: 'szpital połozniczy'
    },
    {
        word: 'mother-to-be',
        translate: 'przyszła matka'
    },
    {
        word: 'motherhood',
        translate: 'macierzyństwo'
    },
    {
        word: 'motherly',
        translate: 'macierzyński'
    },
    {
        word: 'nappy',
        translate: 'pieluszka'
    },
    {
        word: 'nappy rash',
        translate: 'odparzenie od pieluszki'
    },
    {
        word: 'natural childbirth',
        translate: 'poród naturalny'
    },
    {
        word: 'postnatal',
        translate: 'poprodowy'
    },
    {
        word: 'pram',
        translate: 'wózek dziecięcy'
    },
    {
        word: 'pregnancy',
        translate: 'ciąza'
    },
    {
        word: 'pregnancy test',
        translate: 'test ciązowy'
    },
    {
        word: 'pregnant',
        translate: 'w ciązy'
    },
    {
        word: 'pregnant woman',
        translate: 'kobieta w ciązy'
    },
    {
        word: 'premature baby',
        translate: 'wcześniak'
    },
    {
        word: 'quadruplets',
        translate: 'czworaczki'
    },
    {
        word: 'quintuplets',
        translate: 'pięcioraczki'
    },
    {
        word: 'rompers',
        translate: 'pajacyk (ubranie)'
    },
    {
        word: 'seesaw',
        translate: 'huśtawka'
    },
    {
        word: 'spade',
        translate: 'łopatka'
    },
    {
        word: 'to spank',
        translate: 'dać klapsa'
    },
    {
        word: 'stuffed toy',
        translate: 'pluszowa zabawka'
    },
    {
        word: 'to suckle',
        translate: 'karmić piersią'
    },
    {
        word: 'test-tube baby',
        translate: 'dziecko z probówki'
    },
    {
        word: 'the child is in my care',
        translate: 'dziecko jest pod moją opieką'
    },
    {
        word: 'three months pregnant',
        translate: 'w trzecim miesiącu ciązy'
    },
    {
        word: 'toddler',
        translate: 'brzdąc'
    },
    {
        word: 'tot',
        translate: 'szkrab'
    },
    {
        word: 'toy',
        translate: 'zabawka'
    },
    {
        word: 'toy car',
        translate: 'samochodzik'
    },
    {
        word: 'toy soldier',
        translate: 'zołnierzyk'
    },
    {
        word: 'triplets',
        translate: 'trojaczki'
    },
], [
    {
        word: 'a bridal gown',
        translate: 'suknia ślubna'
    },
    {
        word: 'best man',
        translate: 'druzba'
    },
    {
        word: 'bridal',
        translate: 'ślubny/weselny'
    },
    {
        word: 'bride',
        translate: 'panna młoda'
    },
    {
        word: 'bridegroom',
        translate: 'pan młody'
    },
    {
        word: 'bridesman',
        translate: 'druzba'
    },
    {
        word: 'church wedding',
        translate: 'ślub kościelny'
    },
    {
        word: 'civil wedding',
        translate: 'ślub cywilny'
    },
    {
        word: 'to get married',
        translate: 'brać ślub'
    },
    {
        word: 'gilded invitations',
        translate: 'pozłacane zaproszenia'
    },
    {
        word: 'guests',
        translate: 'goście'
    },
    {
        word: 'marriage certificate',
        translate: 'akt ślubu'
    },
    {
        word: 'to marry',
        translate: 'poślubiać'
    },
    {
        word: 'newly-weds',
        translate: 'nowozeńcy'
    },
    {
        word: 'nuptical',
        translate: 'ślub'
    },
    {
        word: 'royal wedding',
        translate: 'ślub królewski'
    },
    {
        word: 'stag party',
        translate: 'wieczór kawalerski'
    },
    {
        word: 'the hotel`s bridal suite',
        translate: 'apartament dla nowozeńców'
    },
    {
        word: 'they were married in 1990',
        translate: 'pobrali się w 1990'
    },
    {
        word: 'to wed',
        translate: 'zenić się'
    },
    {
        word: 'to wedd',
        translate: 'poślubiać'
    },
    {
        word: 'wedding',
        translate: 'ślub'
    },
    {
        word: 'wedding anniversary',
        translate: 'rocznica ślubu'
    },
    {
        word: 'wedding ceremony',
        translate: 'ceremonia ślubna'
    },
    {
        word: 'wedding day',
        translate: 'dzień ślubu'
    },
    {
        word: 'wedding dress',
        translate: 'suknia ślubna'
    },
    {
        word: 'wedding present',
        translate: 'prezent ślubny'
    },
    {
        word: 'wedding procession',
        translate: 'orszak ślubny'
    },
    {
        word: 'wedding ring',
        translate: 'obrączka ślubna'
    }
], [
    {
        word: 'afterlife',
        translate: 'zycie posmiertne'
    },
    {
        word: 'autopsy',
        translate: 'sekcja zwłok'
    },
    {
        word: 'to be afraid of death',
        translate: 'bać się śmierci'
    },
    {
        word: 'beheading',
        translate: 'ścięcie'
    },
    {
        word: 'to bury',
        translate: 'pogrzebać'
    },
    {
        word: 'coffin',
        translate: 'trumna'
    },
    {
        word: 'to commit suicide',
        translate: 'popełnić samobójstwo'
    },
    {
        word: 'corpse',
        translate: 'zwłoki'
    },
    {
        word: 'to cremate',
        translate: 'skremować'
    },
    {
        word: 'cremation',
        translate: 'kremacja'
    },
    {
        word: 'crematorium',
        translate: 'krematorium'
    },
    {
        word: 'dead body',
        translate: 'zwłoki'
    },
    {
        word: 'dead man',
        translate: 'martwy męzczyzna'
    },
    {
        word: 'dead',
        translate: 'martwy'
    },
    {
        word: 'death by hanging',
        translate: 'śmierć przez powieszenie'
    },
    {
        word: 'death certificate',
        translate: 'akt zgonu'
    },
    {
        word: 'death',
        translate: 'śmierć'
    },
    {
        word: 'deceased',
        translate: 'nieboszczyk'
    },
    {
        word: 'to die',
        translate: 'umrzeć'
    },
    {
        word: 'dying',
        translate: 'umieranie'
    },
    {
        word: 'epitaph',
        translate: 'epitafium'
    },
    {
        word: 'euthanasia',
        translate: 'eutanazja'
    },
    {
        word: 'execution',
        translate: 'egzekucja'
    },
    {
        word: 'fratricide',
        translate: 'bratobójstwo'
    },
    {
        word: 'funeral home',
        translate: 'dom pogrzebowy'
    },
    {
        word: 'funeral',
        translate: 'pogrzeb'
    },
    {
        word: 'funeral procession',
        translate: 'kondukt pogrzebowy'
    },
    {
        word: 'grave',
        translate: 'grób'
    },
    {
        word: 'grave digger',
        translate: 'grabarz'
    },
    {
        word: 'gravestone',
        translate: 'nagrobek'
    },
    {
        word: 'graveyard',
        translate: 'cmentarz'
    },
    {
        word: 'he died a natural death',
        translate: 'zmarł śmiercią naturalną'
    },
    {
        word: 'he died of cancer',
        translate: 'zmarł na raka'
    },
    {
        word: 'hospice',
        translate: 'hospicjum'
    },
    {
        word: 'to kill',
        translate: 'zabić'
    },
    {
        word: 'mortal',
        translate: 'śmiertelny'
    },
    {
        word: 'national mourning',
        translate: 'załoba narodowa'
    },
    {
        word: 'obituary',
        translate: 'nekrolog'
    },
    {
        word: 'on his deathbed',
        translate: 'na łozu śmierci'
    },
    {
        word: 'patricide',
        translate: 'ojcobójstwo'
    },
    {
        word: 'to perform an autopsy',
        translate: 'przeprowadzić sekcję zwłok'
    },
    {
        word: 'to perish',
        translate: 'zginąć'
    },
    {
        word: 'soul',
        translate: 'dusza'
    },
    {
        word: 'stoning',
        translate: 'ukamienowanie'
    },
    {
        word: 'suicide',
        translate: 'samobójstwo'
    },
    {
        word: 'testament',
        translate: 'testament'
    },
    {
        word: 'tomb',
        translate: 'grobowiec'
    },
    {
        word: 'Tomb of the Unknown Soldier',
        translate: 'Grób Nieznanego Zołnierza'
    },
    {
        word: 'urn',
        translate: 'urna'
    },
    {
        word: 'widow',
        translate: 'wdowa'
    },
    {
        word: 'widower',
        translate: 'wdowiec'
    },
    {
        word: 'will',
        translate: 'ostatnia wola'
    },
    {
        word: 'wreath',
        translate: 'wieniec'
    }
],
[
    {
        word: 'at bedtime',
        translate: 'przed zaśnięciem'
    }, {
        word: 'to be asleep',
        translate: 'spać'
    }, {
        word: 'to bed down',
        translate: 'ułozyć się do spania'
    }, {
        word: 'to doze',
        translate: 'drzemać'
    }, {
        word: 'to doze off',
        translate: 'zdrzemnąć się'
    }, {
        word: 'to drowse',
        translate: 'drzemać'
    }, {
        word: 'drowsy',
        translate: 'senny/śpiący'
    }, {
        word: 'earplugs',
        translate: 'zatyczki do uszu'
    }, {
        word: 'to go to sleep',
        translate: 'zasypiać'
    }, {
        word: 'to have an early night',
        translate: 'połozyć się wcześniej spać'
    }, {
        word: 'to have a nap',
        translate: 'uciąć sobie drzemkę'
    }, {
        word: 'insomnia',
        translate: 'bezsenność'
    }, {
        word: 'it`s bedtime',
        translate: 'pora spać'
    }, {
        word: 'to make the bed',
        translate: 'słać łózko'
    }, {
        word: 'sleep',
        translate: 'sen'
    }, {
        word: 'to sleep',
        translate: 'spać'
    }, {
        word: 'to sleep in',
        translate: 'zaspać'
    }, {
        word: 'to sleep lightly',
        translate: 'mieć lekki sen'
    }, {
        word: 'sleepily',
        translate: 'sennie'
    }, {
        word: 'sleepless',
        translate: 'bezsenny'
    }, {
        word: 'sleeplessness',
        translate: 'bezsenność'
    }, {
        word: 'sleepwalker',
        translate: 'lunatyk'
    }, {
        word: 'sleepy',
        translate: 'śpiący'
    }, {
        word: 'snooze',
        translate: 'drzemka'
    }, {
        word: 'to snooze',
        translate: 'drzemać'
    }, {
        word: 'snore',
        translate: 'chrapanie'
    }, {
        word: 'to snore',
        translate: 'chrapać'
    }, {
        word: 'to supress a yawn',
        translate: 'stłumić ziewnięcie'
    }, {
        word: 'to yawn',
        translate: 'ziewać'
    }, {
        word: 'yawn',
        translate: 'ziewnięcie'
    }
],
[
    {
        word: 'to attend classes',
        translate: 'chodzić na zajęcia'
    },
    {
        word: 'board',
        translate: 'tablica'
    },
    {
        word: 'break',
        translate: 'przerwa'
    },
    {
        word: 'cheat sheet',
        translate: 'ściąga'
    },
    {
        word: 'classroom',
        translate: 'sala lekcyjna'
    },
    {
        word: 'to collect homework',
        translate: 'zebrać zadania domowe'
    },
    {
        word: 'composition',
        translate: 'wypracowanie'
    },
    {
        word: 'demanding',
        translate: 'wymagający'
    },
    {
        word: 'dictation',
        translate: 'dyktando'
    },
    {
        word: 'to do well at school',
        translate: 'dobrze sie uczyć'
    },
    {
        word: 'entrance examination',
        translate: 'egzamin wstępny'
    },
    {
        word: 'essay',
        translate: 'esej'
    },
    {
        word: 'form teacher',
        translate: 'wychowawca klasy'
    },
    {
        word: 'fourth form',
        translate: 'czwarta klasa'
    },
    {
        word: 'good marks',
        translate: 'dobre oceny'
    },
    {
        word: 'grade',
        translate: 'ocena'
    },
    {
        word: 'grammar school',
        translate: 'szkoła średnia'
    },
    {
        word: 'groupwork',
        translate: 'praca w grupach'
    },
    {
        word: 'headmaster',
        translate: 'dyrektor szkoły'
    },
    {
        word: 'homework',
        translate: 'praca domowa'
    },
    {
        word: 'to learn',
        translate: 'uczyć się'
    },
    {
        word: 'lesson',
        translate: 'lekcja'
    },
    {
        word: 'low marks',
        translate: 'słabe oceny'
    },
    {
        word: 'mark',
        translate: 'ocena'
    },
    {
        word: 'to mark',
        translate: 'oceniać'
    },
    {
        word: 'to miss classes',
        translate: 'opuszczać lekcje'
    },
    {
        word: 'pairwork',
        translate: 'praca w parach'
    },
    {
        word: 'to play hooky',
        translate: 'chodzić na wagary'
    },
    {
        word: 'primary school',
        translate: 'szkoła podstawowa'
    },
    {
        word: 'public school',
        translate: 'elitarna szkoła prywatna'
    },
    {
        word: 'pupil',
        translate: 'uczeń'
    },
    {
        word: 'put up one`s hand',
        translate: 'zgłaszać się'
    },
    {
        word: 'to repeat a year',
        translate: 'powtarzać rok'
    },
    {
        word: 'school report',
        translate: 'świadectwo szkolne'
    },
    {
        word: 'schooldays',
        translate: 'szkolne lata'
    },
    {
        word: 'schooling',
        translate: 'nauka szkolna'
    },
    {
        word: 'secondary school',
        translate: 'szkoła średnia'
    },
    {
        word: 'subject of a lesson',
        translate: 'temat lekcji'
    },
    {
        word: 'to take tests',
        translate: 'pisać testy'
    },
    {
        word: 'to teach',
        translate: 'uczyć'
    },
    {
        word: 'teacher',
        translate: 'nauczyciel'
    },
    {
        word: 'teaching aids',
        translate: 'pomoce naukowe'
    },
    {
        word: 'vocational school',
        translate: 'szkoła zawodowa'
    },
    {
        word: 'chancellor',
        translate: 'rektor'
    },
    {
        word: 'colleage',
        translate: 'szkoła wyzsza/uczelnia'
    },
    {
        word: 'dean',
        translate: 'dziekan'
    },
    {
        word: 'examination',
        translate: 'egzamin'
    },
    {
        word: 'to examine',
        translate: 'egzaminować'
    },
    {
        word: 'faculty',
        translate: 'wydział'
    },
    {
        word: 'to fail an exam',
        translate: 'oblać egzamin'
    },
    {
        word: 'graduate',
        translate: 'absolwent'
    },
    {
        word: 'graduate student',
        translate: 'student ostatniego roku'
    },
    {
        word: 'graduate study',
        translate: 'studia podyplomowe'
    },
    {
        word: 'lecture',
        translate: 'wykład'
    },
    {
        word: 'lecturer',
        translate: 'wykładowca'
    },
    {
        word: 'Master`s degree programe',
        translate: 'studia magisterskie'
    },
    {
        word: 'oral examination',
        translate: 'egzamin ustny'
    },
    {
        word: 'to pass an exam',
        translate: 'zdać egzamin'
    },
    {
        word: 'professor',
        translate: 'profesor'
    },
    {
        word: 'semester',
        translate: 'semestr'
    },
    {
        word: 'seminar',
        translate: 'seminarium'
    },
    {
        word: 'study',
        translate: 'studia'
    },
    {
        word: 'to study for an exam',
        translate: 'uczyć się do egzaminu'
    },
    {
        word: 'teacher training college',
        translate: 'kolegium nauczycielskie'
    },
    {
        word: 'university',
        translate: 'uniwersytet'
    },
    {
        word: 'written examination',
        translate: 'egzamin pisemny'
    }
],
[
    {
        word: 'blood',
        translate: 'krew'
    },
    {
        word: 'body',
        translate: 'ciało'
    },
    {
        word: 'bone',
        translate: 'kość'
    },
    {
        word: 'brain',
        translate: 'mózg'
    },
    {
        word: 'cheek',
        translate: 'policzek'
    },
    {
        word: 'ear',
        translate: 'ucho'
    },
    {
        word: 'elbow',
        translate: 'łokieć'
    },
    {
        word: 'eye',
        translate: 'oko'
    },
    {
        word: 'eyebrow',
        translate: 'brew'
    },
    {
        word: 'eyelid',
        translate: 'powieka'
    },
    {
        word: 'face',
        translate: 'twarz'
    },
    {
        word: 'fist',
        translate: 'pięść'
    },
    {
        word: 'foot',
        translate: 'stopa'
    },
    {
        word: 'forehead',
        translate: 'czoło'
    },
    {
        word: 'gum',
        translate: 'dziąsło'
    },
    {
        word: 'hair',
        translate: 'włosy'
    },
    {
        word: 'hand',
        translate: 'ręka'
    },
    {
        word: 'head',
        translate: 'głowa'
    },
    {
        word: 'heart',
        translate: 'serce'
    },
    {
        word: 'hip',
        translate: 'biodro'
    },
    {
        word: 'knee',
        translate: 'kolano'
    },
    {
        word: 'larynx',
        translate: 'krtań'
    },
    {
        word: 'leg',
        translate: 'noga'
    },
    {
        word: 'liver',
        translate: 'wątroba'
    },
    {
        word: 'lip',
        translate: 'warga'
    },
    {
        word: 'lung',
        translate: 'płuco'
    },
    {
        word: 'mouth',
        translate: 'usta'
    },
    {
        word: 'muscle',
        translate: 'mięsień'
    },
    {
        word: 'nail',
        translate: 'paznokieć'
    },
    {
        word: 'naval',
        translate: 'pępek'
    },
    {
        word: 'neck',
        translate: 'szyja'
    },
    {
        word: 'nose',
        translate: 'nos'
    },
    {
        word: 'palm',
        translate: 'dłoń'
    },
    {
        word: 'part of the body',
        translate: 'część ciała'
    },
    {
        word: 'prostate',
        translate: 'prostata'
    },
    {
        word: 'rib',
        translate: 'zebro'
    },
    {
        word: 'skin',
        translate: 'skóra'
    },
    {
        word: 'stomach',
        translate: 'zołądek'
    },
    {
        word: 'thigh',
        translate: 'udo'
    },
    {
        word: 'thyroid',
        translate: 'tarczyca'
    },
    {
        word: 'tongue',
        translate: 'język'
    },
    {
        word: 'tooth',
        translate: 'ząb'
    },
    {
        word: 'vein',
        translate: 'zyła'
    }
],
[
    {
        word: 'accurate',
        translate: 'dokładny'
    },
    {
        word: 'aggressive',
        translate: 'agresywny'
    },
    {
        word: 'ambitious',
        translate: 'ambitny'
    },
    {
        word: 'bore',
        translate: 'nudziarz'
    },
    {
        word: 'boring',
        translate: 'nudny'
    },
    {
        word: 'carefree',
        translate: 'beztroski'
    },
    {
        word: 'character',
        translate: 'charakter'
    },
    {
        word: 'cheeky',
        translate: 'bezczelny'
    },
    {
        word: 'clever',
        translate: 'zdolny'
    },
    {
        word: 'conceited',
        translate: 'zarozumiały'
    },
    {
        word: 'courageous',
        translate: 'odwazny'
    },
    {
        word: 'coward',
        translate: 'tchórz'
    },
    {
        word: 'cowardly',
        translate: 'tchórzliwy'
    },
    {
        word: 'cruel',
        translate: 'orkutny'
    },
    {
        word: 'dishonest',
        translate: 'nieuczciwy'
    },
    {
        word: 'faithful',
        translate: 'wierny'
    },
    {
        word: 'feature',
        translate: 'cecha'
    },
    {
        word: 'friendly',
        translate: 'przyjazny'
    },
    {
        word: 'full of energy',
        translate: 'energiczny'
    },
    {
        word: 'generous',
        translate: 'wielkoduszny'
    },
    {
        word: 'genuine',
        translate: 'szczery'
    },
    {
        word: 'honest',
        translate: 'uczciwy'
    },
    {
        word: 'incurable optimist',
        translate: 'niepoprawny optymista'
    },
    {
        word: 'intellectual',
        translate: 'intelektualista'
    },
    {
        word: 'introvert',
        translate: 'introwertyk'
    },
    {
        word: 'lazy',
        translate: 'leniwy'
    },
    {
        word: 'lazybones',
        translate: 'leń'
    },
    {
        word: 'liar',
        translate: 'kłamca'
    },
    {
        word: 'mean',
        translate: 'skąpy'
    },
    {
        word: 'modest',
        translate: 'skromny'
    },
    {
        word: 'nervous',
        translate: 'nerwowy'
    },
    {
        word: 'nice',
        translate: 'miły'
    },
    {
        word: 'non-stop talker',
        translate: 'gaduła'
    },
    {
        word: 'nosy',
        translate: 'wścibski'
    },
    {
        word: 'optimist',
        translate: 'optymista'
    },
    {
        word: 'patient',
        translate: 'cierpliwy'
    },
    {
        word: 'personality',
        translate: 'osobowość'
    },
    {
        word: 'pessimist',
        translate: 'pesymista'
    },
    {
        word: 'pleasant',
        translate: 'przyjemny'
    },
    {
        word: 'polite',
        translate: 'uprzejmy'
    },
    {
        word: 'responsible',
        translate: 'odpowiedzialny'
    },
    {
        word: 'shy',
        translate: 'nieśmiały'
    },
    {
        word: 'sociable',
        translate: 'towarzyski'
    },
    {
        word: 'strange',
        translate: 'dziwny'
    },
    {
        word: 'stubborn',
        translate: 'uparty'
    },
    {
        word: 'stupid',
        translate: 'głupi'
    },
    {
        word: 'tactful',
        translate: 'taktowny'
    },
    {
        word: 'tidy',
        translate: 'porządny'
    },
    {
        word: 'tolerant',
        translate: 'tolerancyjny'
    },
    {
        word: 'truthful',
        translate: 'prawdomówny'
    },
    {
        word: 'warm-hearted',
        translate: 'serdeczny'
    }
],
[
    {
        word: 'a bit mad',
        translate: 'lekko stuknięty'
    },
    {
        word: 'absent-minded',
        translate: 'roztargniony'
    },
    {
        word: 'abstemious',
        translate: 'wstrzemięźliwy'
    },
    {
        word: 'acquisitive',
        translate: 'zachłanny'
    },
    {
        word: 'alarmist',
        translate: 'panikarz'
    },
    {
        word: 'aloof',
        translate: 'powściągliwy'
    },
    {
        word: 'aloofness',
        translate: 'powściągliwość'
    },
    {
        word: 'amiable',
        translate: 'miły'
    },
    {
        word: 'appointments are deceptive',
        translate: 'pozory mylą'
    },
    {
        word: 'arrogance',
        translate: 'arogancja'
    },
    {
        word: 'arrogant',
        translate: 'arogancki'
    },
    {
        word: 'avarice',
        translate: 'skąpstwo'
    },
    {
        word: 'avaricious',
        translate: 'skąpy'
    },
    {
        word: 'bad guy',
        translate: 'czarny charakter'
    },
    {
        word: 'barefaced',
        translate: 'bezczelny'
    },
    {
        word: 'bone idle',
        translate: 'leń śmierdzący'
    },
    {
        word: 'brash',
        translate: 'zuchwały'
    },
    {
        word: 'brazen',
        translate: 'bezwstydny'
    },
    {
        word: 'to brazen it out',
        translate: 'nadrabiać tupetem'
    },
    {
        word: 'callous',
        translate: 'bezduszny'
    },
    {
        word: 'callousness',
        translate: 'bezduszność'
    },
    {
        word: 'capricious',
        translate: 'kapryśny'
    },
    {
        word: 'carefree',
        translate: 'beztroski'
    },
    {
        word: 'cast od mind',
        translate: 'uosobienie'
    },
    {
        word: 'conceit',
        translate: 'zarozumiałość'
    },
    {
        word: 'conceited',
        translate: 'zarozumiały'
    },
    {
        word: 'courage',
        translate: 'odwaga'
    },
    {
        word: 'courageous',
        translate: 'odwazny'
    },
    {
        word: 'dastardly',
        translate: 'nikczemny'
    },
    {
        word: 'devious',
        translate: 'przebiegły'
    },
    {
        word: 'dishonest',
        translate: 'nieuczciwy'
    },
    {
        word: 'dishonesty',
        translate: 'nieuczciwość'
    },
    {
        word: 'dotty',
        translate: 'stuknięty'
    },
    {
        word: 'effeminate',
        translate: 'zniewieściały'
    },
    {
        word: 'effusive',
        translate: 'wylewny'
    },
    {
        word: 'evil incarnate',
        translate: 'wcielenie zła'
    },
    {
        word: 'forbearing',
        translate: 'wyrozumiały'
    },
    {
        word: 'fortitude',
        translate: 'hart ducha'
    },
    {
        word: 'full of ideas',
        translate: 'pełen pomysłów'
    },
    {
        word: 'full of oneself',
        translate: 'zarozumiały'
    },
    {
        word: 'good-for nothing',
        translate: 'nicpoń'
    },
    {
        word: 'good-natured',
        translate: 'dobroduszny'
    },
    {
        word: 'grasping',
        translate: 'zachłanny'
    },
    {
        word: 'guileless',
        translate: 'prostoduszny'
    },
    {
        word: 'guilty conscience',
        translate: 'nieczyste sumienie'
    },
    {
        word: 'gullbility',
        translate: 'łatwowierność'
    },
    {
        word: 'gullible',
        translate: 'łatwowierny'
    },
    {
        word: 'headstrong',
        translate: 'zawzięty'
    },
    {
        word: 'hot-blooded',
        translate: 'krewki'
    },
    {
        word: 'hot-tempered',
        translate: 'porywczy'
    },
    {
        word: 'idealist',
        translate: 'idealista'
    },
    {
        word: 'idleness',
        translate: 'próżniactwo'
    },
    {
        word: 'idler',
        translate: 'próżniak'
    },
    {
        word: 'ill-natured',
        translate: 'złośliwy'
    },
    {
        word: 'impressionable',
        translate: 'łatwowierny'
    },
    {
        word: 'imprudent',
        translate: 'nieroztropny'
    },
    {
        word: 'indecisive',
        translate: 'niezdecydowany'
    },
    {
        word: 'to judge by appointments',
        translate: 'sądzić po pozorach'
    },
    {
        word: 'kind-hearted',
        translate: 'życzliwy'
    },
    {
        word: 'kindred spirit',
        translate: 'bratnia dusza'
    },
    {
        word: 'laziness',
        translate: 'lenistwo'
    },
    {
        word: 'loner',
        translate: 'samotnik'
    },
    {
        word: 'man of few words',
        translate: 'małomówny'
    },
    {
        word: 'maverick',
        translate: 'indywidualista'
    },
    {
        word: 'merciless',
        translate: 'bezlitosny'
    },
    {
        word: 'mindless',
        translate: 'bezmyślny'
    },
    {
        word: 'miserly',
        translate: 'skąpy'
    },
    {
        word: 'modest',
        translate: 'skromny'
    },
    {
        word: 'modesty',
        translate: 'skromność'
    },
    {
        word: 'nosy',
        translate: 'wścibski'
    },
    {
        word: 'prig',
        translate: 'zarozumialec'
    },
    {
        word: 'ratty',
        translate: 'nerwowy'
    },
    {
        word: 'reckluse',
        translate: 'odludek'
    },
    {
        word: 'resourceful',
        translate: 'pomysłowy'
    },
    {
        word: 'resourcefulness',
        translate: 'pomysłowość'
    },
    {
        word: 'revengeful',
        translate: 'mściwy'
    },
    {
        word: 'ruthless',
        translate: 'bezwzględny'
    },
    {
        word: 'ruthlessness',
        translate: 'bezwzględność'
    },
    {
        word: 'scanty',
        translate: 'skąpy'
    },
    {
        word: 'screwy',
        translate: 'selfish'
    },
    {
        word: 'selfishness',
        translate: 'samolubstwo'
    },
    {
        word: 'selfless',
        translate: 'bezinteresowny'
    },
    {
        word: 'sensibility',
        translate: 'wrażliwość'
    },
    {
        word: 'sharp-tempered',
        translate: 'porywczy'
    },
    {
        word: 'shrewd',
        translate: 'przebiegły'
    },
    {
        word: 'shy',
        translate: 'nieśmiały'
    },
    {
        word: 'shyly',
        translate: 'nieśmiało'
    },
    {
        word: 'shyness',
        translate: 'nieśmiałość'
    },
    {
        word: 'sincere',
        translate: 'szczery'
    },
    {
        word: 'sincerity',
        translate: 'szczerość'
    },
    {
        word: 'Sleeeping Beauty',
        translate: 'Śpiąca Królewna'
    },
    {
        word: 'snappy',
        translate: 'zgryźliwy'
    },
    {
        word: 'spiteful',
        translate: 'złośliwy'
    },
    {
        word: 'surly',
        translate: 'opryskliwy'
    },
    {
        word: 'tenacious',
        translate: 'uparty'
    },
    {
        word: 'tenacity',
        translate: 'upartość'
    },
    {
        word: 'to treat sb like dirt',
        translate: 'traktować kogoś jak szmatę'
    },
    {
        word: 'vindictive',
        translate: 'mściwy'
    },
    {
        word: 'warm-hearted',
        translate: 'serdeczny'
    },
    {
        word: 'well-mannered',
        translate: 'dobrze wychowany'
    }
],
[
    {
        word: 'a man of medium height',
        translate: 'mężczyzna średniego wzrostu'
    },
    {
        word: 'a week`s growth of beard',
        translate: 'tygodniowy zarost'
    },
    {
        word: 'apperance',
        translate: 'wygląd'
    },
    {
        word: 'athletic',
        translate: 'wysportowany'
    },
    {
        word: 'attractive',
        translate: 'atrakcyjny'
    },
    {
        word: 'auburn hair',
        translate: 'kasztanowe włosy'
    },
    {
        word: 'bags under the eyes',
        translate: 'worki pod oczami'
    },
    {
        word: 'baldness',
        translate: 'łysina'
    },
    {
        word: 'bandy-legged',
        translate: 'krzywonogi'
    },
    {
        word: 'be bleary eyed',
        translate: 'mieć zaczerwienione oczy'
    },
    {
        word: 'be bow-legged',
        translate: 'mieć pałąkowate nogi'
    },
    {
        word: 'beard',
        translate: 'broda'
    },
    {
        word: 'beautiful',
        translate: 'piękny'
    },
    {
        word: 'become thin',
        translate: 'chudnąć'
    },
    {
        word: 'black and blue',
        translate: 'posiniaczony'
    },
    {
        word: 'blackhead',
        translate: 'wągier'
    },
    {
        word: 'blond',
        translate: 'blondyn'
    },
    {
        word: 'blonde',
        translate: 'blondynka'
    },
    {
        word: 'braid',
        translate: 'warkoczyk'
    },
    {
        word: 'brawn',
        translate: 'krzepa'
    },
    {
        word: 'charm',
        translate: 'wdzięk'
    },
    {
        word: 'chubby',
        translate: 'pucułowaty'
    },
    {
        word: 'corpulent',
        translate: 'korpulentny'
    },
    {
        word: 'dark hair',
        translate: 'ciemne włosy'
    },
    {
        word: 'to depilate',
        translate: 'depilować'
    },
    {
        word: 'to do one`s hair',
        translate: 'układać sobie włosy'
    },
    {
        word: 'elegant',
        translate: 'elegancki'
    },
    {
        word: 'facial expression',
        translate: 'wyraz twarzy'
    },
    {
        word: 'fair-haired',
        translate: 'jasnowłosy'
    },
    {
        word: 'fat',
        translate: 'gruby'
    },
    {
        word: 'feature',
        translate: 'cecha'
    },
    {
        word: 'figure',
        translate: 'cecha'
    },
    {
        word: 'freckle',
        translate: 'pieg'
    },
    {
        word: 'freckled',
        translate: 'piegowaty'
    },
    {
        word: 'fringe',
        translate: 'grzywka'
    },
    {
        word: 'frizzy hair',
        translate: 'kręcone włosy'
    },
    {
        word: 'full in the face',
        translate: 'pełna twarz'
    },
    {
        word: 'gain weight',
        translate: 'przybrać na wadze'
    },
    {
        word: 'girl with blue eyes',
        translate: 'dziewczyna  niebieskich oczach'
    },
    {
        word: 'good-looking',
        translate: 'przystojny'
    },
    {
        word: 'grace',
        translate: 'gracja'
    },
    {
        word: 'handsome',
        translate: 'przystojny'
    },
    {
        word: 'he has a black eye',
        translate: 'ma podbite oko'
    },
    {
        word: 'he looks his age',
        translate: 'wygląda jak na swój wiek'
    },
    {
        word: 'height',
        translate: 'wzrost'
    },
    {
        word: 'high forehead',
        translate: 'wysokie czoło'
    },
    {
        word: 'his hairline was recending',
        translate: 'robiły mu się zakola'
    },
    {
        word: 'hump',
        translate: 'garb'
    },
    {
        word: 'hunchback',
        translate: 'garbus'
    },
    {
        word: 'hunched',
        translate: 'zgarbiony'
    },
    {
        word: 'let one`s hair down',
        translate: 'rozpuszczać włosy'
    },
    {
        word: 'long-haired',
        translate: 'długowłosy'
    },
    {
        word: 'look smart',
        translate: 'wyglądać elegancko'
    },
    {
        word: 'look tired',
        translate: 'wyglądać na zmęczonego'
    },
    {
        word: 'look-alike',
        translate: 'sobowtór'
    },
    {
        word: 'looks don`t count',
        translate: 'uroda to nie wszystko'
    },
    {
        word: 'to lose one`s (good) looks',
        translate: 'tracić na urodzie'
    },
    {
        word: 'to lose weight',
        translate: 'tracić na wadze'
    },
    {
        word: 'make-up',
        translate: 'makijaż'
    },
    {
        word: 'matronly',
        translate: 'przy kości'
    },
    {
        word: 'moustache',
        translate: 'wąs'
    },
    {
        word: 'neatness',
        translate: 'schludność'
    },
    {
        word: 'obese',
        translate: 'otyły'
    },
    {
        word: 'obesity',
        translate: 'otyłość'
    },
    {
        word: 'of medium build',
        translate: 'średniej budowy'
    },
    {
        word: 'outward apperance',
        translate: 'wygląd zewnętrzny'
    },
    {
        word: 'overwieight',
        translate: 'nadwaga'
    },
    {
        word: 'pale',
        translate: 'blady'
    },
    {
        word: 'pimple',
        translate: 'pryszcz'
    },
    {
        word: 'pimply',
        translate: 'pryszczaty'
    },
    {
        word: 'plait',
        translate: 'warkocz'
    },
    {
        word: 'ponytail',
        translate: 'koński ogon'
    },
    {
        word: 'pretty',
        translate: 'ładny'
    },
    {
        word: 'portruding ears',
        translate: 'odstające uszy'
    },
    {
        word: 'to put on weight',
        translate: 'przybierać na wadze'
    },
    {
        word: 'scar',
        translate: 'blizna'
    },
    {
        word: 'scruffy',
        translate: 'niechlujny'
    },
    {
        word: 'shapely',
        translate: 'zgrabny'
    },
    {
        word: 'shaven',
        translate: 'ogolony'
    },
    {
        word: 'short',
        translate: 'niski'
    },
    {
        word: 'sideburns',
        translate: 'bokobrody'
    },
    {
        word: 'silkhouette',
        translate: 'sylwetka'
    },
    {
        word: 'slender',
        translate: 'smukły'
    },
    {
        word: 'slightly build',
        translate: 'drobnej budowy'
    },
    {
        word: 'slim',
        translate: 'szczupły'
    },
    {
        word: 'smile',
        translate: 'uśmiech'
    },
    {
        word: 'to smile',
        translate: 'uśmiechać się'
    },
    {
        word: 'spotty',
        translate: 'pryszczaty'
    },
    {
        word: 'squint',
        translate: 'zez'
    },
    {
        word: 'stocky',
        translate: 'krępy'
    },
    {
        word: 'stout',
        translate: 'tęgi'
    },
    {
        word: 'strong',
        translate: 'silny'
    },
    {
        word: 'suntan',
        translate: 'opalenizna'
    },
    {
        word: 'suntanned',
        translate: 'opalony'
    },
    {
        word: 'tall',
        translate: 'wysoki'
    },
    {
        word: 'tatoo',
        translate: 'tatuaż'
    },
    {
        word: 'tawny',
        translate: 'śniady'
    },
    {
        word: 'they all wore beards',
        translate: 'wszyscy byli brodaci'
    },
    {
        word: 'they looked tired',
        translate: 'wyglądali na zmęczonych'
    },
    {
        word: 'tiny',
        translate: 'drobny'
    },
    {
        word: 'turned up nose',
        translate: 'zadarty nos'
    },
    {
        word: 'ugly',
        translate: 'brzydki'
    },
    {
        word: 'underweight',
        translate: 'niedowaga'
    },
    {
        word: 'weak',
        translate: 'słaby'
    },
    {
        word: 'weight',
        translate: 'waga'
    },
    {
        word: 'well-dressed',
        translate: 'dobrze ubrany'
    },
    {
        word: 'wrinkle',
        translate: 'zmarszczka'
    },
    {
        word: 'wrinkled',
        translate: 'pomarszczony'
    }
],
[
    {
        word: 'a gift for languages',
        translate: 'talent do języków'
    },
    {
        word: 'a word to the wise',
        translate: 'mądrej głowie dość dwie słowie'
    },
    {
        word: 'ably',
        translate: 'umiejętnie'
    },
    {
        word: 'to absorb knowledge',
        translate: 'chłonąć wiedzę'
    },
    {
        word: 'act the fool',
        translate: 'udawać głupiego'
    },
    {
        word: 'analysis',
        translate: 'analiza'
    },
    {
        word: 'aptitude',
        translate: 'zdolność'
    },
    {
        word: 'aptitude test',
        translate: 'test zdolności'
    },
    {
        word: 'to argue',
        translate: 'argumentować'
    },
    {
        word: 'argument',
        translate: 'argument'
    },
    {
        word: 'associacion',
        translate: 'skojarzenie'
    },
    {
        word: 'to be a slow learner',
        translate: 'wolno się uczyć'
    },
    {
        word: 'to be mistaken',
        translate: 'być w błędzie'
    },
    {
        word: 'bigheaded',
        translate: 'przemądrzały'
    },
    {
        word: 'blithering idiot',
        translate: 'skończony idiota'
    },
    {
        word: 'boffin',
        translate: 'jajogłowy'
    },
    {
        word: 'brains of canary',
        translate: 'ptasi móżdżek'
    },
    {
        word: 'brainwave',
        translate: 'olśnienie'
    },
    {
        word: 'brainy',
        translate: 'bystry'
    },
    {
        word: 'brilliant',
        translate: 'błyskotliwy'
    },
    {
        word: 'to broaden sb`s mind',
        translate: 'poszerzać czyjeś horyzonty'
    },
    {
        word: 'catch question',
        translate: 'podchwytliwe pytanie'
    },
    {
        word: 'clever',
        translate: 'bystry'
    },
    {
        word: 'clueless',
        translate: 'ciężko myślący'
    },
    {
        word: 'to collect one`s thoughts',
        translate: 'zebrać myśli'
    },
    {
        word: 'comprehensive',
        translate: 'wszechstronny'
    },
    {
        word: 'conclusion',
        translate: 'wniosek'
    },
    {
        word: 'cretin',
        translate: 'kretyn'
    },
    {
        word: 'to deduce',
        translate: 'wnioskować'
    },
    {
        word: 'deduction',
        translate: 'wnioskowanie'
    },
    {
        word: 'deep in thought',
        translate: 'zamyślony'
    },
    {
        word: 'difficult to understand',
        translate: 'trudny do zrozumienia'
    },
    {
        word: 'to discover',
        translate: 'odkrywać'
    },
    {
        word: 'discovery',
        translate: 'odkrycie'
    },
    {
        word: 'discussion',
        translate: 'dyskusja'
    },
    {
        word: 'duffer',
        translate: 'beztalencie'
    },
    {
        word: 'dull',
        translate: 'tępy'
    },
    {
        word: 'educated',
        translate: 'wykształcony'
    },
    {
        word: 'empty-headed',
        translate: 'głupiutki'
    },
    {
        word: 'erudite',
        translate: 'erudyta'
    },
    {
        word: 'erudition',
        translate: 'erudycja'
    },
    {
        word: 'experience',
        translate: 'doświadczenie'
    },
    {
        word: 'experienced',
        translate: 'doświadczony'
    },
    {
        word: 'feeble-minded',
        translate: 'słaby na umyśle'
    },
    {
        word: 'fool',
        translate: 'głupiec'
    },
    {
        word: 'foolish',
        translate: 'głupi'
    },
    {
        word: 'foolishness',
        translate: 'głupota'
    },
    {
        word: 'forget',
        translate: 'zapomnieć'
    },
    {
        word: 'from what I know',
        translate: 'z tego, co wiem'
    },
    {
        word: 'genius',
        translate: 'geniusz'
    },
    {
        word: 'gifted',
        translate: 'utalentowany'
    },
    {
        word: 'to have no idea',
        translate: 'nie mieć pojęcia'
    },
    {
        word: 'he is good at Italian',
        translate: 'jest dobry z włoskiego'
    },
    {
        word: 'he`s got brains',
        translate: 'ma dobrą głowę'
    },
    {
        word: 'hypothesis',
        translate: 'hipoteza'
    },
    {
        word: 'idea',
        translate: 'myśl'
    },
    {
        word: 'ignorance',
        translate: 'niewiedza'
    },
    {
        word: 'ignorant',
        translate: 'niedouczony'
    },
    {
        word: 'illerate',
        translate: 'analfabeta'
    },
    {
        word: 'imbecile',
        translate: 'imbecyl'
    },
    {
        word: 'in blissful ignorance',
        translate: 'w błogiej niewiedzy'
    },
    {
        word: 'ingenious',
        translate: 'pomysłowy'
    },
    {
        word: 'ingenuity',
        translate: 'pomysłowość'
    },
    {
        word: 'inquiring',
        translate: 'dociekliwy'
    },
    {
        word: 'intellect',
        translate: 'intelekt'
    },
    {
        word: 'intellectual',
        translate: 'intelektualista'
    },
    {
        word: 'intelligence test',
        translate: 'test na inteligencję'
    },
    {
        word: 'intelligent',
        translate: 'inteligentny'
    },
    {
        word: 'intelligently',
        translate: 'inteligentnie'
    },
    {
        word: 'intuition',
        translate: 'intuicja'
    },
    {
        word: 'inventive',
        translate: 'pomysłowy'
    },
    {
        word: 'inventiveness',
        translate: 'pomysłowość'
    },
    {
        word: 'jerk',
        translate: 'palant'
    },
    {
        word: 'know-all',
        translate: 'mądrala'
    },
    {
        word: 'knowledge',
        translate: 'wiedza'
    },
    {
        word: 'lack of education',
        translate: 'brak wykształcenia'
    },
    {
        word: 'learned',
        translate: 'uczony'
    },
    {
        word: 'logic',
        translate: 'logika'
    },
    {
        word: 'logical',
        translate: 'logiczny'
    },
    {
        word: 'memory',
        translate: 'pamięć'
    },
    {
        word: 'mental block',
        translate: 'zaćmienie umysłu'
    },
    {
        word: 'narrow-minded',
        translate: 'ograniczony'
    },
    {
        word: 'noodle',
        translate: 'głąb'
    },
    {
        word: 'on the assumption',
        translate: 'przy założeniu'
    },
    {
        word: 'presence of mind',
        translate: 'przytomność umysłu'
    },
    {
        word: 'quickly of mind',
        translate: 'bystrość umysłu'
    },
    {
        word: 'reason',
        translate: 'rozum'
    },
    {
        word: 'right',
        translate: 'racja'
    },
    {
        word: 'self-taught person',
        translate: 'samouk'
    },
    {
        word: 'sense',
        translate: 'sens'
    },
    {
        word: 'simpleton',
        translate: 'prostak'
    },
    {
        word: 'stupid',
        translate: 'głupi'
    },
    {
        word: 'stupidity',
        translate: 'głupota'
    },
    {
        word: 'talented',
        translate: 'utalentowany'
    },
    {
        word: 'thesis',
        translate: 'teza'
    },
    {
        word: 'thinker',
        translate: 'myśliciel'
    },
    {
        word: 'thinking',
        translate: 'myślenie'
    },
    {
        word: 'thoughtless',
        translate: 'bezmyślny'
    },
    {
        word: 'twerp',
        translate: 'przygłup'
    },
    {
        word: 'use your brains',
        translate: 'rusz głową'
    },
    {
        word: 'well-read',
        translate: 'oczytany'
    },
    {
        word: 'wise',
        translate: 'mądry'
    }
],
[
    {
        word: 'addiction',
        translate: 'nałóg'
    },
    {
        word: 'addiction to drugs',
        translate: 'lekomania'
    },
    {
        word: 'alcohol',
        translate: 'alkohol'
    },
    {
        word: 'alcoholic',
        translate: 'alkoholik'
    },
    {
        word: 'ashtry',
        translate: 'popielniczka'
    },
    {
        word: 'to become addicted to',
        translate: 'uzależniać się od czegoś'
    },
    {
        word: 'beer',
        translate: 'piwo'
    },
    {
        word: 'beer can',
        translate: 'puszka po piwie'
    },
    {
        word: 'bottle',
        translate: 'butelka'
    },
    {
        word: 'brandy',
        translate: 'brandy'
    },
    {
        word: 'champagne',
        translate: 'szampan'
    },
    {
        word: 'cheroot',
        translate: 'cygaro'
    },
    {
        word: 'chew tobacco',
        translate: 'żuć tytoń'
    },
    {
        word: 'cigar',
        translate: 'cygaro'
    },
    {
        word: 'cigarette case',
        translate: 'papierośnica'
    },
    {
        word: 'cigarette end',
        translate: 'niedopałek'
    },
    {
        word: 'cigarette holder',
        translate: 'cygarniczka'
    },
    {
        word: 'coffe',
        translate: 'kawa'
    }
]
]