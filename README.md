#### O metodzie pięciu żołądków
Metoda pięciu żołądków jest sposobem uczenia się słówek za pomocą fiszek, ale w określonej formule podzielonej na serie ćwiczeniowe (odstępy między ćwiczeniami są dowolne, najlepiej jeden lub kilka dni). Na początku nauki wszyskie fiszki trafiają do pierwszego żołądka (takim żołądkiem może być pudełko, lub przegródka). Bierzemy po kolei fiszki i jeśli prawidłowo odgadujemy słowo, trafia ono do następnego żołądka, jeśli nie zgadniemy wraca na koniec pierwszego żołądka. Podczas kolejnej sesji robimy to samo, jeśli zgadniemy słówko z drugiego żołądka trafia do trzeciego, jeśli nie to wraca do pierwszego. Jeśli zgadniemy słówko z piątego żołądka słówko uznajemy z zapamiętane i możemy je wyrzucić. W ten sposób mamy pewność że każde słówko wielokrotnie zostało powtórzone.

### Cele projektu
Celem projektu jest stworzenie aplikacji na **Androida**, która będzie umożliwiała użytkownikowi uczenie się słówek języka obcego za pomocą metody pięciu żołądków. Baza słówek jest tworzona ręcznie przez użytkownika, dla lepszego rozróżnienia każde słówko ma swoją kategorię. Słówka można edytować, lub usuwać.

### Ekran główny - wybór czynności
Na pierwszym ekranie decydujemy czy chcemy:
- dodać nowe słówko lub wyrażenie (można też dodać kategorię)
- rozpocząć naukę słówek
- edytować / usunąć słówka, wyrażenia lub kategorie
Cały widok składa się z tytułu i 3 buttonów

### Ekran dodawania słówek
Na tym ekranie dodajemy słówko. Słówka oraz kategorie trzymamy w lokalnej bazie danych zaimplementowanej w bibliotece **Realm**. Widok składa się z trzech inputów:
- słówko
- tłumaczenie
- nowa kategoria

Ostatni input można przełączyć specjalnym radio buttonem na autocomplete select, z kó†rego możemy wybrać kategorię z już istniejących. Oprócz tego na dole mamy button służący do zatwierdzenia utworzenia nowego słówka, który dodatkowo przeniesie nas na główną stronę

### Ekran edycji słówek / kategorii
Ten ekran służy do zarządzania słówkami i kategoriami. Na górze mamy radio button krórym możemy wybrać czy chcemy edytować słówko czy kategorię. Po wybraniu buttona od słówek zauważymy na ekranie listę słówek. Obok każdego słówka będą dwa buttony: jeden do edycji, drugi do usuwania. Ten do edycji otworzy modala w którym możemy zmienić dane słówka, czyli nazwę, tłumaczenie i kategorię. Podobnie będzie z edycją kategorii, przy usunięciu kategorii usunięte zostaną również wszystkie przypisane do niej słówka.

### Ekran początkowy nauki
Na tym ekranie decydujemy czy chcemy dodać słówka z jakiejś kategorii do pierwszego żołądka (jeśli jakaś kategoria była wcześniej wybrana, a potem dodano do niej słówka, można te nowe na tym etapie też dodać do pierwszego żołądka). Zatwierdzamy wybór kategorii za pomocą buttona

### Ekran żołądków
Na początku wyświetla nam się słówko z pierwszego żołądka. Udzielamy odpowiedzi po czym sprawdzamy jakie jest tłumaczenie za pomocą buttona "Sprawdź". Po sprawdzeniu czy nasza odpowiedź jest prawidłowa zatwierdzamy ją zielonym buttonem co przenosi słówko do kolejnego żołądka, jeśli klikniemy czerwony button słówko wróci do pierwszego żołądka zgodnie z zasadami. Nie wiem co się ma dziać ze słówkiem po piątym żołądku, teoretycznie powinienem je usunąć, ale na razie będzie wisiało i może dorobię opcję resetowania progresu, żeby można było do tych słówek wracać. Ekran również posiada dwa buttony umożliwiające przechodzenie między żołądkami

