
import {CategorySchema, WordSchema} from './components/schemas';

export const changeStomachNumberCategory = (oldNumber, newNumber, categoryName) => {
    Realm.open({schema: [CategorySchema, WordSchema]}).then((realm) => {
        realm.write(() => { 
            let editedCategory = realm.objectForPrimaryKey('Category', categoryName)
            let changeOldNumber = editedCategory[oldNumber] - 1
            let changeNewNumber = editedCategory[newNumber] + 1
            realm.create('Category', {id: categoryName, [oldNumber]: changeOldNumber, [newNumber]: changeNewNumber}, 'modified');
        })
        realm.close();
      });
}